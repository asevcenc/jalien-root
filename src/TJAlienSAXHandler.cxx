#include "TJAlienSAXHandler.h"

TJAlienSAXHandler::TJAlienSAXHandler(TJAlienResult* result) {
  fJAliEnResult = result;
}

void TJAlienSAXHandler::OnStartDocument() {
  Info("TJAlien", "start XML document");
}

//SAX Handler
void TJAlienSAXHandler::OnEndDocument() {
  Info("TJAlien", "end XML document");
}

void TJAlienSAXHandler::OnStartElement(const char* name, const TList* attributes) {
  Info("TJAlienSAXHandler", "OnStartElement event: entering");

  // Element name
  TString startEl(name);

  TIter next(attributes);
  // vyurchen: Could not change this pointer into unique_ptr
  // No problem on the build machine, but in CI: "undefined symbol: _ZTV8TXMLAttr"
  TXMLAttr *attr;

  // Check if the element is the "document" element containing the meta data
  if (!startEl.CompareTo("document")) {
		while ((attr = (TXMLAttr*) next())) {
      Info("TJAlienSAXHandler", "Attribute name= %s and attribute value=%s", attr->GetName(), attr->GetValue());

      Info("TJAlienSAXHandler", "Adding element to the meta hash");
      fJAliEnResult->SetMetaData(attr->GetName(), attr->GetValue());
    }
  }
  // The element is a result element, adding it to TJAlienResult
  else {
    TMap* element = new TMap();
    element->SetOwnerKeyValue(kTRUE, kTRUE);
		while ((attr = (TXMLAttr*) next())) {
      Info("TJAlienSAXHandler", "Attribute name= %s and attribute value=%s", attr->GetName(), attr->GetValue());

      auto attrName = new TObjString(attr->GetName());
      auto attrValue = new TObjString(attr->GetValue());

      Info("TJAlienSAXHandler", "Adding element to the JAliEnResult");
      element->Add(attrName, attrValue);
    }

    fJAliEnResult->Add(element);
  }
  Info("TJAlienSAXHandler", "OnStartElement event: exiting");
}

void TJAlienSAXHandler::OnEndElement(const char* name) {
  Info("TJAlien", "</ %s >", name);
}

void TJAlienSAXHandler::OnCharacters(const char* characters) {
  Info("TJAlien", "%s", characters);
}

void TJAlienSAXHandler::OnComment(const char* text) {
  Info("TJAlien", "<!--%s -->", text);
}

void TJAlienSAXHandler::OnWarning(const char* text) {
  Warning("TJAlien", "%s", text);
}

void TJAlienSAXHandler::OnError(const char* text) {
  Error("TJAlien", "%s", text);
}

void TJAlienSAXHandler::OnFatalError(const char* text) {
  Fatal("TJAlien", "%s", text);
}

void TJAlienSAXHandler::OnCdataBlock(const char* text, Int_t len) {
  UNUSED(text);
  UNUSED(len);
  Info("TJAlien", "OnCdataBlock()");
}

TJAlienSAXHandler::~TJAlienSAXHandler() = default;
