// @(#)root/base:$Id$
// Author: Andreas Peters   15/05/2006

/*************************************************************************
 * Copyright (C) 1995-2006, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienSystem                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TJAlienSystem.h"
#include "TError.h"
#include "TUrl.h"
#include "TGrid.h"
#include "TJAlien.h"

ClassImp(TJAlienSystem)

//______________________________________________________________________________
TJAlienSystem::TJAlienSystem(const char* name, const char* title) : TSystem(name, title) {
  // Create a new OS interface.
}

//______________________________________________________________________________
TJAlienSystem::~TJAlienSystem() = default;

//______________________________________________________________________________
Bool_t TJAlienSystem::Init() {
  // Initialize the OS interface.
  return kTRUE;
}

//______________________________________________________________________________
int TJAlienSystem::MakeDirectory(const char* dirname) {
  // Make a directory. Returns 0 in case of success and
  // -1 if the directory could not be created (either already exists or
  // illegal path name).

  if (!gGrid) {
    return -1;
  }

  if (strcmp(gGrid->GetGrid(), "alien") != 0) {
    Error("TJAlienSystem", "You are not connected to AliEn");
    return -1;
  }

  TUrl url(dirname);
  url.CleanRelativePath();
  if (strcmp(url.GetProtocol(), "alien") != 0) {
    Info("OpenDirectory", "Assuming an AliEn URL alien://%s", dirname);
    url.SetProtocol("alien", kTRUE);
  }
  return gGrid->Mkdir(url.GetFile());
}

//______________________________________________________________________________
void* TJAlienSystem::OpenDirectory(const char* name) {
  // Open a directory. Returns 0 if directory does not exist.
  UNUSED(name);
  (dynamic_cast<TJAlien*>(gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
  return nullptr;
  /*TUrl url(name);
    url.CleanRelativePath();
    if (strcmp(url.GetProtocol(), "alien")) {
        Info("OpenDirectory", "Assuming an AliEn URL alien://%s",name);
        url.SetProtocol("alien", kTRUE);
    }
    return (void*) jalien_opendir(url.GetUrl());
    */
}

//______________________________________________________________________________
void TJAlienSystem::FreeDirectory(void* ptr) {
  // Free a directory.
  UNUSED(ptr);
  (dynamic_cast<TJAlien*>(gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
}

//______________________________________________________________________________
const char* TJAlienSystem::GetDirEntry(void* ptr) {
  // Get a directory entry. Returns 0 if no more entries.

  UNUSED(ptr);
  (dynamic_cast<TJAlien*>(gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
  /*struct dirent* retdir;
    retdir = gapi_readdir( (GAPI_DIR*) ptr);
    //   AbstractMethod("GetDirEntry");
    if (retdir)
      return retdir->d_name;
    */
  return nullptr;
}

//______________________________________________________________________________
Bool_t TJAlienSystem::ChangeDirectory(const char* dirname) {
  // Change directory.
  TUrl url(dirname);
  url.CleanRelativePath();
  if (strcmp(url.GetProtocol(), "alien") != 0) {
    Info("OpenDirectory", "Assuming an AliEn URL alien://%s", dirname);
    url.SetProtocol("alien", kTRUE);
  }
  return gGrid->Cd(url.GetFile(), kFALSE);
}

//______________________________________________________________________________
const char* TJAlienSystem::WorkingDirectory() {
  // Return working directory.
  return gGrid->Pwd();
}

//______________________________________________________________________________
const char* TJAlienSystem::HomeDirectory(const char*) {
  // Return the user's home directory.
  if (!gGrid) {
    return nullptr;
  }

  if (strcmp(gGrid->GetGrid(), "alien") != 0) {
    Error("TJAlienSystem", "You are not connected to AliEn");
    return nullptr;
  }
  return (gGrid->GetHomeDirectory());
}

//______________________________________________________________________________
int TJAlienSystem::mkdir(const char* name, Bool_t recursive) {
  // Make a file system directory. Returns 0 in case of success and
  // -1 if the directory could not be created (either already exists or
  // illegal path name).
  // If 'recursive' is true, makes parent directories as needed.

  if (recursive) {
    TString dirname = DirName(name);
    if (dirname.Length() == 0) {
      // well we should not have to make the root of the file system!
      // (and this avoid infinite recursions!)
      return -1;
    }
    if (AccessPathName(dirname, kFileExists)) {
      int res = mkdir(dirname, kTRUE);
      if (res) {
        return res;
      }
    }
    if (!AccessPathName(name, kFileExists)) {
      return -1;
    }
  }

  return MakeDirectory(name);
}

//______________________________________________________________________________
int TJAlienSystem::CopyFile(const char*, const char*, Bool_t) {
  // Copy a file. If overwrite is true and file already exists the
  // file will be overwritten. Returns 0 when successful, -1 in case
  // of failure, -2 in case the file already exists and overwrite was false.

  AbstractMethod("CopyFile");
  return -1;
}

//______________________________________________________________________________
int TJAlienSystem::Rename(const char* oldname, const char* newname) {
  // Rename a file.
  UNUSED(oldname);
  UNUSED(newname);
  (dynamic_cast<TJAlien*>(gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
  return -1;
  //return gapi_rename(oldname,newname);
  //  AbstractMethod("Rename");
  //   return -1;
}

//______________________________________________________________________________
int TJAlienSystem::Link(const char*, const char*) {
  // Create a link from file1 to file2.

  AbstractMethod("Link");
  return -1;
}

//______________________________________________________________________________
int TJAlienSystem::Symlink(const char*, const char*) {
  // Create a symbolic link from file1 to file2.

  AbstractMethod("Symlink");
  return -1;
}

//______________________________________________________________________________
int TJAlienSystem::Unlink(const char* filename) {
  // Unlink, i.e. remove, a file.
  if (gGrid->Rm(filename)) {
    return 0;
  }
  else {
    return -1;
  }
}

//______________________________________________________________________________
int TJAlienSystem::GetPathInfo(const char* path, FileStat_t& buf) {
  // Get info about a file. Info is returned in the form of a FileStat_t
  // structure (see TSystem.h).
  // The function returns 0 in case of success and 1 if the file could
  // not be stat'ed.

  //   AbstractMethod("GetPathInfo(const char*, FileStat_t&)");
  UNUSED(path);
  UNUSED(buf);
  (dynamic_cast<TJAlien*>(gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
  return 0;
}

//______________________________________________________________________________
int TJAlienSystem::GetFsInfo(const char*, Long_t*, Long_t*, Long_t*, Long_t*) {
  // Get info about a file system: fs type, block size, number of blocks,
  // number of free blocks.

  AbstractMethod("GetFsInfo");
  return 1;
}

//______________________________________________________________________________
int TJAlienSystem::Chmod(const char* file, UInt_t mode) {
  // Set the file permission bits. Returns -1 in case or error, 0 otherwise.
  UNUSED(file);
  UNUSED(mode);
  (dynamic_cast<TJAlien*>(gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
  return 0;
  /*TUrl url(file);
    url.CleanRelativePath();
    if (strcmp(url.GetProtocol(),"alien")) {
        Info("AlienFilestat", "Assuming an AliEn URL alien://%s",file);
        url.SetProtocol("alien",kTRUE);
    }
    return gapi_chmod(url.GetUrl(),mode);*/
}

//______________________________________________________________________________
int TJAlienSystem::Umask(Int_t) {
  // Set the process file creation mode mask.

  AbstractMethod("Umask");
  return -1;
}

//______________________________________________________________________________
int TJAlienSystem::Utime(const char*, Long_t, Long_t) {
  // Set the a files modification and access times. If actime = 0 it will be
  // set to the modtime. Returns 0 on success and -1 in case of error.

  AbstractMethod("Utime");
  return -1;
}

//______________________________________________________________________________
const char* TJAlienSystem::FindFile(const char*, TString&, EAccessMode) {
  // Find location of file in a search path.
  // Returns 0 in case file is not found.

  AbstractMethod("Which");
  return nullptr;
}

//______________________________________________________________________________
Bool_t TJAlienSystem::AccessPathName(const char* path, EAccessMode mode) {
  // Returns FALSE if one can access a file using the specified access mode.
  // The file name must not contain any special shell characters line ~ or $,
  // in those cases first call ExpandPathName().
  // Attention, bizarre convention of return value!!
  UNUSED(mode);
  // (dynamic_cast<TJAlien*> (gGrid))->NotImplemented(__func__, __FILE__, __LINE__);
  // return kFALSE;
  if (!gGrid) {
    return -1;
  }

  if (strcmp(gGrid->GetGrid(), "alien") != 0) {
    Error("TJAlienSystem", "You are not connected to AliEn");
    return -1;
  }

  TString strippath = path;
  // remove trailing '/'
  while (strippath.EndsWith("/")) {
    strippath.Remove(strippath.Length() - 1);
  }
  TUrl url(strippath);
  url.CleanRelativePath();

  if (strcmp(url.GetProtocol(), "alien") != 0) {
    Info("AccessPathName", "Assuming an AliEn URL alien://%s", path);
    url.SetProtocol("alien", kTRUE);
  }

  TString sPath = TString(url.GetUrl()).ReplaceAll("alien://", "");
  auto result =
    std::unique_ptr<TJAlienResult>(dynamic_cast<TJAlienResult*>(gGrid->Command(TString("stat") + " " + sPath)));
  if (!result) {
    Error("TJAlienSystem", "Empty TJAlienResult");
  }
  TString val = result->GetKey(0, "__result__");

  bool returnval = true;
  if (strcmp(val, "1") == 0) {
    returnval = false;
  }
  return returnval;
}

//---- Users & Groups ----------------------------------------------------------

//______________________________________________________________________________
Int_t TJAlienSystem::GetUid(const char* /*user*/) {
  // Returns the user's id. If user = 0, returns current user's id.

  AbstractMethod("GetUid");
  return 0;
}

//______________________________________________________________________________
Int_t TJAlienSystem::GetEffectiveUid() {
  // Returns the effective user id. The effective id corresponds to the
  // set id bit on the file being executed.

  AbstractMethod("GetEffectiveUid");
  return 0;
}

//______________________________________________________________________________
Int_t TJAlienSystem::GetGid(const char* /*group*/) {
  // Returns the group's id. If group = 0, returns current user's group.

  AbstractMethod("GetGid");
  return 0;
}

//______________________________________________________________________________
Int_t TJAlienSystem::GetEffectiveGid() {
  // Returns the effective group id. The effective group id corresponds
  // to the set id bit on the file being executed.

  AbstractMethod("GetEffectiveGid");
  return 0;
}

//______________________________________________________________________________
UserGroup_t* TJAlienSystem::GetUserInfo(Int_t /*uid*/) {
  // Returns all user info in the UserGroup_t structure. The returned
  // structure must be deleted by the user. In case of error 0 is returned.

  AbstractMethod("GetUserInfo");
  return nullptr;
}

//______________________________________________________________________________
UserGroup_t* TJAlienSystem::GetUserInfo(const char* /*user*/) {
  // Returns all user info in the UserGroup_t structure. If user = 0, returns
  // current user's id info. The returned structure must be deleted by the
  // user. In case of error 0 is returned.

  AbstractMethod("GetUserInfo");
  return nullptr;
}

//______________________________________________________________________________
UserGroup_t* TJAlienSystem::GetGroupInfo(Int_t /*gid*/) {
  // Returns all group info in the UserGroup_t structure. The only active
  // fields in the UserGroup_t structure for this call are:
  //    fGid and fGroup
  // The returned structure must be deleted by the user. In case of
  // error 0 is returned.

  AbstractMethod("GetGroupInfo");
  return nullptr;
}

//______________________________________________________________________________
UserGroup_t* TJAlienSystem::GetGroupInfo(const char* /*group*/) {
  // Returns all group info in the UserGroup_t structure. The only active
  // fields in the UserGroup_t structure for this call are:
  //    fGid and fGroup
  // If group = 0, returns current user's group. The returned structure
  // must be deleted by the user. In case of error 0 is returned.

  AbstractMethod("GetGroupInfo");
  return nullptr;
}
