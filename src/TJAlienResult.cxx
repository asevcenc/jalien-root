// @(#)root/alien:$Id$
// Author: Fons Rademakers   23/5/2002

/*************************************************************************
 * Copyright (C) 1995-2000, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAliEnResult                                                        //
//                                                                      //
// Class defining interface to a JAlien result set.                     //
// Objects of this class are created by TGrid methods.                  //
//                                                                      //
// Related classes are TJAlien.                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TJAlienResult.h"
#include "TObjString.h"
#include "TMap.h"
#include "Riostream.h"
#include "TSystem.h"
#include "TUrl.h"
#include "TFileInfo.h"
#include "TEntryList.h"
#include <cstdlib>

ClassImp(TJAlienResult)

TJAlienResult::TJAlienResult() = default;

//______________________________________________________________________________
// container of mother class (TList) are cleaned up
// because they are owners of all pointers
TJAlienResult::~TJAlienResult() = default;

//______________________________________________________________________________
void TJAlienResult::DumpResult() {
  // Dump result set.

  //cout << "BEGIN DUMP" << endl;
  Info("TJAlien", "BEGIN DUMP");
  TIter next(this);
  while (auto map = dynamic_cast<TMap*>(next())) {
    TIter next2 = map->GetTable();
    while (auto pair = dynamic_cast<TPair*>(next2())) {
      auto keyStr = dynamic_cast<TObjString*>(pair->Key());
      auto valueStr = dynamic_cast<TObjString*>(pair->Value());

      if (keyStr) {
        //std::cout << "Key: " << keyStr->GetString() << "   ";
        //Info("TJAlien","Key: %s   ",keyStr->GetString());
        Info("TJAlien", "Key: %s  ", keyStr->GetName());
      }
      if (valueStr) {
        //std::cout << "Value: " << valueStr->GetString();
        Info("TJAlien", "Value: %s", valueStr->GetName());
      }
      //std::cout << endl;
      Info("TJAlien", "\n");
    }
  }

  //cout << "END DUMP" << endl;
  Info("TJAlien", "END DUMP");
}

//______________________________________________________________________________
const char* TJAlienResult::GetFileName(UInt_t i) const {
  //Return a file name.

  if (At(i)) {
    if (auto entry = dynamic_cast<TObjString*>(dynamic_cast<TMap*>(At(i))->GetValue("name"))) {
      return entry->GetName();
    }
  }
  return nullptr;
}

//______________________________________________________________________________
const TEntryList* TJAlienResult::GetEntryList(UInt_t i) const {
  // Return the entry list, if evtlist was defined as a tag.

  if (At(i)) {
    if (auto entry = dynamic_cast<TEntryList*>(dynamic_cast<TMap*>(At(i))->GetValue("evlist"))) {
      return entry;
    }
  }
  return nullptr;
}

//______________________________________________________________________________
const char* TJAlienResult::GetFileNamePath(UInt_t i) const {
  // Return file name path.

  if (At(i)) {
    if (auto entry = dynamic_cast<TEntryList*>(dynamic_cast<TMap*>(At(i))->GetValue("name"))) {
      if (auto path = dynamic_cast<TObjString*>(dynamic_cast<TMap*>(At(i))->GetValue("path"))) {
        fFilePath = TString(path->GetName()) + TString(entry->GetName());
        return fFilePath;
      }
    }
  }
  return nullptr;
}

//______________________________________________________________________________
const char* TJAlienResult::GetPath(UInt_t i) const {
  // Return path.

  if (At(i)) {
    if (auto entry = dynamic_cast<TObjString*>(dynamic_cast<TMap*>(At(i))->GetValue("path"))) {
      return entry->GetName();
    }
  }
  return nullptr;
}

//______________________________________________________________________________
const char* TJAlienResult::GetKey(UInt_t i, const char* key) const {
  // Return the key.

  if (At(i)) {
    if (auto entry = dynamic_cast<TObjString*>(dynamic_cast<TMap*>(At(i))->GetValue(key))) {
      return entry->GetName();
    }
  }
  return nullptr;
}

//______________________________________________________________________________
Bool_t TJAlienResult::SetKey(UInt_t i, const char* key, const char* value) {
  // Set the key.

  if (At(i)) {
    if (auto entry = dynamic_cast<TPair*>(dynamic_cast<TMap*>(At(i))->FindObject(key))) {
      dynamic_cast<TMap*>(At(i))->Remove(dynamic_cast<TObject*>(entry->Key()));
    }
    dynamic_cast<TMap*>(At(i))->Add(new TObjString(key), new TObjString(value));
    return kTRUE;
  }
  return kFALSE;
}

//______________________________________________________________________________
TList* TJAlienResult::GetFileInfoList() const {
  // Return a file info list.

  auto newfileinfolist = std::make_unique<TList>();

  newfileinfolist->SetOwner(kTRUE);

  for (Int_t i = 0; i < GetSize(); i++) {
    Long64_t size = -1;
    if (GetKey(i, "size")) {
      size = strtol(GetKey(i, "size"), nullptr, 0);
    }

    TString md5 = GetKey(i, "md5");
    TString uuid = GetKey(i, "guid");
    TString msd = GetKey(i, "msd");

    if (!md5.IsNull() && md5.Length() == 0) {
      md5 = nullptr;
    }
    if (!uuid.IsNull() && uuid.Length() == 0) {
      uuid = nullptr;
    }
    if (!msd.IsNull() && msd.Length() == 0) {
      msd = nullptr;
    }

    TString turl = GetKey(i, "turl");

    if (msd) {
      TUrl urlturl(turl);
      TString options = urlturl.GetOptions();
      options += "&msd=";
      options += msd;
      urlturl.SetOptions(options);
      turl = urlturl.GetUrl();
    }
    Info("GetFileInfoList", "Adding Url %s with Msd %s\n", turl.Data(), msd.Data());
    newfileinfolist->Add(new TFileInfo(turl, size, uuid, md5));
  }
  return newfileinfolist.release();
}

//______________________________________________________________________________
void TJAlienResult::Print(Option_t* option) const {
  // Print the AlienResult info.

  Long64_t totaldata = 0;
  Int_t totalfiles = 0;

  if (TString(option) != TString("all")) {
    // the default print out format is for a query
    for (Int_t i = 0; i < GetSize(); i++) {
      if (TString(option) == TString("l")) {
        printf("( %06d ) LFN: %-80s   Size[Bytes]: %10s   GUID: %s\n", i, GetKey(i, "lfn"), GetKey(i, "size"),
               GetKey(i, "guid"));
      }
      else {
        printf("( %06d ) LFN: .../%-48s   Size[Bytes]: %10s   GUID: %s\n", i, gSystem->BaseName(GetKey(i, "lfn")),
               GetKey(i, "size"), GetKey(i, "guid"));
      }
      if (GetKey(i, "size")) {
        totaldata += strtol(GetKey(i, "size"), nullptr, 0);
        totalfiles++;
      }
    }
    printf("------------------------------------------------------------\n");
    printf("-> Result contains %.02f MB in %d Files.\n", static_cast<double>(totaldata) / 1024. / 1024., totalfiles);
  }
  else {
    TIter next(this);
    Int_t i = 1;
    while (auto map = dynamic_cast<TMap*>(next())) {
      TIter next2(map->GetTable());
      printf("------------------------------------------------------------\n");
      while (auto pair = dynamic_cast<TPair*>(next2())) {
        auto keyStr = dynamic_cast<TObjString*>(pair->Key());
        auto valueStr = dynamic_cast<TObjString*>(pair->Value());
        if (keyStr && valueStr) {
          printf("( %06d ) [ -%16s ]  = %s\n", i, keyStr->GetName(), valueStr->GetName());
        }
      }
      i++;
    }
  }
}

//______________________________________________________________________________
std::string TJAlienResult::GetMetaData(const std::string& key) const {
  return fJAliEnMetaData.at(key);
}

void TJAlienResult::SetMetaData(const std::string& key, const std::string& value) {
  fJAliEnMetaData.insert({key, value});
}

void TJAlienResult::Add(TObject* obj) {
  // in principle only TMaps should be added, and they
  // must own the key and value objects for safe cleanup during destruction
  auto map = dynamic_cast<TMap*>(obj);
  if (!map) {
    Error("Add", "Only TMaps can be added to TJAlienResult");
    return;
  }
  if (!(map->IsOwnerValue() && map->IsOwner())) {
    Warning("Add", "Only TMaps owning key and value pointers should be added. Otherwise this object might leak");
  }
  TGridResult::Add(obj);
}
