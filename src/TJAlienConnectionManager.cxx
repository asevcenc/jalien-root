// @(#)root/net:$Id$
// Author: Volodymyr Yurchenko 2019 - 2023

#include "TJAlienConnectionManager.h"
#include <iostream>
#include <uv.h>

ClassImp(TJAlienConnectionManager)

int TJAlienConnectionManager::destroy_flag = 0;
int TJAlienConnectionManager::connection_flag = 0;
int TJAlienConnectionManager::writeable_flag = 0;
int TJAlienConnectionManager::receive_flag = 0;
std::string TJAlienConnectionManager::readBuffer;

const int TJAlienConnectionManager::lws_ping_interval =
  std::getenv("JALIEN_PING_INTERVAL") ? std::min(std::stoi(std::getenv("JALIEN_PING_INTERVAL")), 32000) :
                                        default_ping_interval;
const int TJAlienConnectionManager::lws_timeout =
  std::getenv("JALIEN_PING_TIMEOUT") ? std::min(std::stoi(std::getenv("JALIEN_PING_TIMEOUT")), 32000) : default_timeout;

// Use existing foreign loop. In case there is none, a new loop will be created
uv_loop_t* TJAlienConnectionManager::loop = uv_default_loop();

const lws_retry_bo_t TJAlienConnectionManager::retry = {
  .secs_since_valid_ping = static_cast<uint16_t>(TJAlienConnectionManager::lws_ping_interval),
  .secs_since_valid_hangup = static_cast<uint16_t>(TJAlienConnectionManager::lws_timeout),
};

//------------------------------------------------------------------------------
// TJAlienConnectionManager::TJAlienConnectionManager()
//------------------------------------------------------------------------------
TJAlienConnectionManager::~TJAlienConnectionManager() {
  if (context) {
    lws_context_destroy(context);
  }

  if (creds.has(cJOB_TOKEN)) {
    creds.removeCredentials(cJOB_TOKEN);
  }
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::CreateConnection()
//
// Decides how to connect: JBox or JCentral, token or grid certificate
// Returns -1 if failed, 0 if JBox, 1 if JCentral with full grid certificate, 2 if JCentral with token
//------------------------------------------------------------------------------
int TJAlienConnectionManager::CreateConnection() {
  TJAlienCredentialsObject co;
  TJAlienDNSResolver dns_jcentral(default_server, default_WSport);
  std::string current_host;

  clearFlags();

  creds.loadCredentials();
  if (creds.count() == 0) {
    Error("TJAlienConnectionManager", "Failed to get any credentials");
    return -1;
  }

  while (creds.count() > 0) {
    if (creds.has(cJOB_TOKEN)) {
      co = creds.get(cJOB_TOKEN);
    }
    else if (creds.has(cJBOX_TOKEN)) {
      co = creds.get(cJBOX_TOKEN);
    }
    else if (creds.has(cFULL_GRID_CERT)) {
      co = creds.get(cFULL_GRID_CERT);
      if (co.password.empty()) {
        co.readPassword();
      }
    }
    else {
      Error("TJAlienConnectionManager", "Failed to get any credentials");
      return -1;
    }

    if (co.kind == cJBOX_TOKEN || co.kind == cJOB_TOKEN) {
      ConnectJBox(co);
    }

    if (connection_flag) {
      Info("TJAlienConnectionManager", "Successfully connected to JBox");
      co.password = "";
      fWSHost = "localhost";
      return 0;
    }

    // std::cout << "\r" << "Opening connection to JCentral. Please wait" << std::flush;
    Info("TJAlienConnectionManager", "Opening connection to JCentral. Please wait");
    for (int i = 0; i < dns_jcentral.length(); i++) {
      // std::cout << "." << std::flush;
      current_host = dns_jcentral.get_next_host();
      Info("TJAlienConnectionManager", "Opening connection to %s", current_host.c_str());
      ConnectJCentral(co, current_host);

      if (connection_flag) {
        // std::cout << "\r" << std::flush;
        Info("TJAlienConnectionManager", "Successfully connected to %s", current_host.c_str());
        co.password = "";
        fWSHost = default_server;
        if (co.kind == cFULL_GRID_CERT) {
          return 1;
        }
        else {
          return 2;
        }
      }
      else {
        if (gDebug > 0) {
          // std::cout << "\r" << std::flush;
          Error("TJAlienConnectionManager", "Failed to connect to %s - retrying...", current_host.c_str());
        }
        sleep(1);
      }
    }
    creds.removeCredentials(co.kind);
  }
  // std::cout << "\r" << std::flush;
  Error("TJAlienConnectionManager", "Failed to connect to any server! Giving up");
  return -1;
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::clearFlags()
//
// Clears all flags used by the connection
//------------------------------------------------------------------------------
void TJAlienConnectionManager::clearFlags() {
  destroy_flag = 0;
  connection_flag = 0;
  writeable_flag = 0;
  receive_flag = 0;
  readBuffer = "";
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::ConnectJBox()
//
// Create the connection to JBox using the parameters read from jclient_token_xxx file
//------------------------------------------------------------------------------
void TJAlienConnectionManager::ConnectJBox(const TJAlienCredentialsObject& c) {
  // Try to load host and port env vars
  std::string jboxHost = std::getenv("JALIEN_HOST") ?: "";
  int jboxPort = std::getenv("JALIEN_WSPORT") ? std::stoi(std::getenv("JALIEN_WSPORT")) : 0;

  if (jboxHost.length() == 0 || jboxPort == 0) {
    // Try to find jclient_token file
    TJClientFile jcf;
    if (jcf.isValid) {
      jboxHost = jcf.fHost.Data();
      jboxPort = jcf.fWSPort;
    }
    else if (gDebug >= 1) {
      Info("TJAlienConnectionManager", "The JClient file is not valid - not connecting to JBox!");
    }
  }

  if (jboxHost.length() == 0 || jboxPort == 0) {
    if (gDebug >= 1) {
      Info("TJAlienConnectionManager", "Failed to find any local JBox endpoint");
    }
    return;
  }

  MakeWebsocketConnection(c, jboxHost, jboxPort);
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::ConnectJCentral()
//
// Create the connection to JCentral using the default parameters
//------------------------------------------------------------------------------
void TJAlienConnectionManager::ConnectJCentral(const TJAlienCredentialsObject& c, const string& host) {
  if (gDebug > 1) {
    Info("TJAlienConnectionManager", "Trying to connect to server %s", host.c_str());
  }
  MakeWebsocketConnection(c, host, default_WSport);
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::MakeWebsocketConnection()
//
// Set up libwebsockets variables and create the connection
//------------------------------------------------------------------------------
void TJAlienConnectionManager::MakeWebsocketConnection(const TJAlienCredentialsObject& credentialsObject,
                                                       const string& host,
                                                       int WSPort) {
  bool is_ipv4 = host.find('.') != std::string::npos;

  if (gDebug > 0) {
    Info("TJAlienConnectionManager", "Connecting to Server %s:%d", host.c_str(), WSPort);
    Info("TJAlienConnectionManager", "Using cert %s and %s", credentialsObject.certpath.c_str(),
         credentialsObject.keypath.c_str());
  }

  // Use this for debugging
  // lws_set_log_level(LLL_EXT | LLL_USER | LLL_PARSER | LLL_INFO | LLL_ERR | LLL_NOTICE, nullptr);
  lws_set_log_level(gDebug > 0 ? 2047 : 0, nullptr);

  // Reset context variables
  context = nullptr;
  wsi = nullptr;

  clearFlags();

  // libwebsockets variables
  struct lws_client_connect_info connect_info {};

  struct lws_context_creation_info creation_info {};  // Info to create logical connection

  memset(&connect_info, 0, sizeof connect_info);
  memset(&creation_info, 0, sizeof creation_info);

  // SSL options
  int use_ssl =
    LCCSCF_USE_SSL | LCCSCF_SKIP_SERVER_CERT_HOSTNAME_CHECK;  // SSL, no selfsigned, don't check server hostname

  // Define protocol
  static const struct lws_protocols protocols[] = {
    {"jalien-protocol", ws_service_callback, 0, 0, 1, nullptr},
    {nullptr,           nullptr,             0, 0, 0, nullptr}  /* end */
  };

  // Create the websockets context. This tracks open connections and
  // knows how to route any traffic and which protocol version to use,
  // and if each connection is client or server side.
  creation_info.port = CONTEXT_PORT_NO_LISTEN;  // NO_LISTEN - we are client
  creation_info.iface = nullptr;
  creation_info.protocols = protocols;
  creation_info.extensions = nullptr;
  creation_info.gid = -1;
  creation_info.uid = -1;
  creation_info.options = 0;
  creation_info.vhost_name = "tjalien-root";
#ifndef LWS_WITH_LIBUV // libuv ignores the LWS_SERVER_OPTION_LIBUV option if LWS_WITH_LIBUV is not set.
#error libwebsockets was not built against libuv, the libuv event loop cannot drive libwebsockets
#endif
  creation_info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT | LWS_SERVER_OPTION_LIBUV;
  if (is_ipv4) {
    creation_info.options |= LWS_SERVER_OPTION_DISABLE_IPV6;
  }

  // Connect to the "foreign" uv event loop - it might be a loop used by DPL or a new lws-only loop
  void* foreign_loops[1] = {loop};
  creation_info.foreign_loops = foreign_loops;

  creation_info.client_ssl_cert_filepath = credentialsObject.certpath.c_str();
  creation_info.client_ssl_private_key_filepath = credentialsObject.keypath.c_str();
  creation_info.client_ssl_private_key_password = credentialsObject.password.c_str();

  // Create context - only logical connection, no real connection yet
  context = lws_create_context(&creation_info);
  if (context == nullptr) {
    Error("TJAlienConnectionManager", "Context creation failure");
    destroy_flag = 1;
    return;
  }
  if (gDebug > 1) {
    Info("TJAlienConnectionManager", "context created");
  }

  connect_info.address = host.c_str();
  connect_info.port = WSPort;
  connect_info.path = "/websocket/json";
  connect_info.context = context;
  connect_info.ssl_connection = use_ssl;
  connect_info.host = host.c_str();
  connect_info.origin = host.c_str();
  connect_info.ietf_version_or_minus_one = -1;
  connect_info.protocol = protocols[0].name;
  connect_info.pwsi = &wsi;
  connect_info.retry_and_idle_policy = &retry;

  // Create wsi - WebSocket Instance
  lws_client_connect_via_info(&connect_info);
  if (wsi == nullptr) {
    if (gDebug > 0) {
      Error("TJAlienConnectionManager", "WebSocket instance creation error");
    }
    return;
  }

  if (gDebug > 1) {
    Info("TJAlienConnectionManager", "WebSocket instance creation successful");
  }

  // Wait for server response "connection established"
  while (!connection_flag) {
    uv_run(loop, UV_RUN_ONCE);

    if (destroy_flag) {
      if (gDebug > 1) {
        Error("TJAlienConnectionManager", "Websocket connection failure");
      }
      return;
    }
  }

  if (credentialsObject.kind == cJOB_TOKEN) {
    this->creds.removeCredentials(credentialsObject.kind);
  }

  creation_info.client_ssl_private_key_password = "";
  fWSPort = WSPort;
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::websocket_write_back()
//
// Callback for writing to the websocket
//------------------------------------------------------------------------------
int TJAlienConnectionManager::websocket_write_back(const char* str, int str_size_in) {
  if (str == nullptr || wsi == nullptr) {
    return -1;
  }

  size_t len = (str_size_in < 1) ? strlen(str) : str_size_in;

  auto out = std::make_unique<char[]>(LWS_SEND_BUFFER_PRE_PADDING + len + LWS_SEND_BUFFER_POST_PADDING);
  // Set up the buffer
  std::copy(str, str + len, out.get() + LWS_SEND_BUFFER_PRE_PADDING);
  // Write out
  return lws_write(wsi, reinterpret_cast<unsigned char*>(out.get()) + LWS_SEND_BUFFER_PRE_PADDING, len, LWS_WRITE_TEXT);
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::ws_service_callback()
//
// Callback for servicing the websocket connection
// Returns 0 on success, 1 if credentials load is failed
//------------------------------------------------------------------------------
int TJAlienConnectionManager::ws_service_callback(struct lws* wsi,
                                                  enum lws_callback_reasons reason,
                                                  void* user,
                                                  void* in,
                                                  size_t len) {
  // Websocket callback handler
  switch (reason) {
    case LWS_CALLBACK_CLIENT_ESTABLISHED: {
      if (gDebug > 1) {
        printf("[Websocket Callback] Connect with server success\n");
      }
      connection_flag = 1;
      break;
    }

    case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: {
      if (gDebug > 1) {
        printf("[Websocket Callback] Connect with server error\n");
      }
      destroy_flag = 1;
      connection_flag = 0;
      if (!lws_get_context(wsi)) {
        lws_context_destroy(lws_get_context(wsi));
      }
      wsi = nullptr;
      break;
    }

    case LWS_CALLBACK_CLIENT_CLOSED: {
      if (gDebug > 1) {
        std::cout << "[Websocket Callback] LWS_CALLBACK_CLIENT_CLOSED\n";
      }
      destroy_flag = 1;
      connection_flag = 0;
      if (!lws_get_context(wsi)) {
        lws_context_destroy(lws_get_context(wsi));
      }
      wsi = nullptr;
      break;
    }

    case LWS_CALLBACK_CLIENT_RECEIVE: {
      if (gDebug > 100) {
        std::cout << "[Websocket Callback] Client received: " << static_cast<char*>(in) << '\n';
        std::cout << "[Websocket Callback]: " << len << " (rpp " << lws_remaining_packet_payload(wsi) << ", last "
                  << lws_is_final_fragment(wsi) << ")\n";
      }

      readBuffer.append(static_cast<char*>(in));
      if (lws_is_final_fragment(wsi) != 0) {
        receive_flag = 1;
      }
      len = 0;
      break;
    }

    case LWS_CALLBACK_CLIENT_WRITEABLE: {
      if (gDebug > 1) {
        std::cout << "[Websocket Callback] On writable is called\n";
      }
      writeable_flag = 1;
      break;
    }

#if defined(LWS_OPENSSL_SUPPORT)
    case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS: {
      if (gDebug > 1) {
        std::cout << "[Websocket Callback] LOAD_EXTRA_CLIENT_VERIFY_CERTS is called\n";
      }

      std::string location = "/etc/grid-security/certificates/";

      struct stat info {};

      if (stat("/cvmfs/alice.cern.ch", &info) == 0) {
        location = "/cvmfs/alice.cern.ch" + location;
      }

      std::string capath = std::getenv("X509_CERT_DIR") ?: location;
      size_t pos = 0;
      std::string token;

      // If capath contains two paths separated by ":"
      while ((pos = capath.find(':')) != std::string::npos) {
        token = capath.substr(0, pos);
        if (!SSL_CTX_load_verify_locations(static_cast<SSL_CTX*>(user), nullptr, token.c_str())) {
          if (gDebug > 1) {
            std::cout << "[Websocket Callback] LOAD_EXTRA_CLIENT_VERIFY_CERTS failed\n";
          }
          return 1;
        }
        capath.erase(0, pos + 1);
      }

      // If capath is a single path
      if (capath.length() != 0) {
        if (!SSL_CTX_load_verify_locations(static_cast<SSL_CTX*>(user), nullptr, capath.c_str())) {
          if (gDebug > 1) {
            std::cout << "[Websocket Callback] LOAD_EXTRA_CLIENT_VERIFY_CERTS failed\n";
          }
          return 1;
        }
      }
      break;
    }

    case LWS_CALLBACK_OPENSSL_PERFORM_SERVER_CERT_VERIFICATION: {
      if (gDebug > 1) {
        std::cout << "[Websocket Callback] LWS_CALLBACK_OPENSSL_PERFORM_SERVER_CERT_VERIFICATION is called\n";
      }
      break;
    }

#endif

    default:
      break;
  }
  return 0;
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::ForceRestart()
//
// Immediately breaks previous connection and starts a new one with user grid certificate
//------------------------------------------------------------------------------
void TJAlienConnectionManager::ForceRestart() {
  destroy_flag = 1;
  connection_flag = 0;
  if (context) {
    lws_context_destroy(context);
  }

  ConnectJCentral(creds.get(cFULL_GRID_CERT));
  if (!IsConnected()) {
    Info("TJAlienConnectionManager", "Failed to establish the connection to the server");
    return;
  }
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::CreateJsonCommand()
//
// Create the TGrid command in JSON format
//------------------------------------------------------------------------------
json_object* TJAlienConnectionManager::CreateJsonCommand(const TString& command, const TList* opt) {
  if (command.Length() == 0) {
    Error("TJAlienConnectionManager::CreateJsonCommand", "Received empty command");
    return nullptr;
  }

  auto jobj = json_object_new_object();
  auto jstringcmd = json_object_new_string(command.Data());
  json_object_object_add(jobj, "command", jstringcmd);
  auto jarray = json_object_new_array();

  if (opt != nullptr && opt->GetEntries() != 0) {
    for (const auto& entry : *opt) {
      auto ovalue = dynamic_cast<TObjString*>(entry);
      auto svalue = ovalue->GetString();
      json_object_array_add(jarray, json_object_new_string(svalue));
    }
  }

  // Disable stdout message output; TJAlien reconstructs stdout from all response values on it's own
  json_object_array_add(jarray, json_object_new_string("-nomsg"));
  json_object_object_add(jobj, "options", jarray);

  return jobj;
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::RunJsonCommand()
//------------------------------------------------------------------------------
std::unique_ptr<TJAlienResult> TJAlienConnectionManager::RunJsonCommand(const TString& command, const TList* opt) {
  std::map<std::string, std::string> emptyMetadata;
  std::string emptyStr;
  return RunJsonCommand(command, opt, emptyMetadata, emptyStr);
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::RunJsonCommand()
//
// Execute the TGrid command (Ls(), Cd(), etc.)
// This is the "libwebsockets backend" for TJAlien::Command()
//------------------------------------------------------------------------------
std::unique_ptr<TJAlienResult> TJAlienConnectionManager::RunJsonCommand(const TString& command,
                                                                        const TList* opt,
                                                                        std::map<std::string, std::string>& metadata,
                                                                        std::string& buffer) {
  auto jsonCommand = CreateJsonCommand(command, opt);

  if (jsonCommand == nullptr) {
    Error("TJAlienConnectionManager", "JSON command build failed: %s", command.Data());
    return nullptr;
  }

  if (gDebug > 1) {
    Info("TJAlienConnectionManager", "JSON command to be done: %s", json_object_to_json_string(jsonCommand));
  }

  if (!IsConnected()) {
    Error("TJAlienConnectionManager", "Connection is broken!");
    return nullptr;
  }

  // Prepare the string to send and wait for the connection to be in a writable state
  readBuffer = "";
  websocket_write_back(json_object_to_json_string(jsonCommand), -1);
  lws_callback_on_writable(wsi);
  while (!receive_flag && !destroy_flag) {
    uv_run(loop, UV_RUN_ONCE);
  }

  // The connection is broken or closed by server, notify TJAlien it has to reconnect
  if (destroy_flag) {
    Info("TJAlienConnectionManager", "Connection should be re-established");
    return nullptr;
  }

  receive_flag = 0;
  json_object_put(jsonCommand);

  auto jobj_res = json_tokener_parse(readBuffer.c_str());

  // Get response
  auto gridResult = std::move(GetCommandResult(jobj_res, command.CompareTo("find") == 0));
  json_object_put(jobj_res);

  buffer = readBuffer;

  // Extract the username, current directory, host and port
  // immediately after each command, since it could change
  metadata["fHost"] = fWSHost;
  metadata["fPort"] = std::to_string(fWSPort);
  if (gridResult) {
    string sUserMetadata = gridResult->GetMetaData("user");
    if (!sUserMetadata.empty()) {
      metadata["fUser"] = sUserMetadata;
    }
    string sPwdMetadata = gridResult->GetMetaData("currentdir");
    if (!sPwdMetadata.empty()) {
      metadata["fPwd"] = sPwdMetadata;
    }
  }
  return gridResult;
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::GetCommandResult()
//
// JSON parser of the command result - parsing the meta information
//------------------------------------------------------------------------------
std::unique_ptr<TJAlienResult> TJAlienConnectionManager::GetCommandResult(json_object* json_response,
                                                                          bool expand_find) {
  if (json_response == nullptr) {
    Error("GetCommandResult", "Result JSON is nullptr");
    return nullptr;
  }

  auto gridResult = std::make_unique<TJAlienResult>();

  // get data from metadata
  json_object* js_metadata;
  json_object_object_get_ex(json_response, "metadata", &js_metadata);
  if (js_metadata != nullptr && json_object_is_type(js_metadata, json_type_object)) {
    json_object_object_foreach(js_metadata, key, val) {
      gridResult->SetMetaData(key, json_object_get_string(val));
    }
  }

  // get data from results
  json_object* js_results;
  json_object_object_get_ex(json_response, "results", &js_results);
  if (js_results != nullptr && json_object_is_type(js_results, json_type_array)) {
    for (int i = 0; i < json_object_array_length(js_results); i++) {
      auto jvalue = json_object_array_get_idx(js_results, i);
      auto t = std::make_unique<TMap>();
      t->SetOwnerKeyValue(kTRUE, kTRUE);
      json_object_object_foreach(jvalue, key, val) {
        TString sValue = json_object_get_string(val);
        TString sKey = key;
        t->Add(new TObjString(key), new TObjString(sValue));
        if (expand_find && sKey == "lfn") {
          t->Add(new TObjString("turl"), new TObjString("alien://" + sValue));
        }
      }
      gridResult->Add(t.release());
    }
  }
  return gridResult;
}

//------------------------------------------------------------------------------
// TJAlienConnectionManager::IsConnected()
//
// Polls the connection to get its status.
// Quickly checks that the connection is alive using the non-blocking UV_RUN_NOWAIT
//------------------------------------------------------------------------------
Bool_t TJAlienConnectionManager::IsConnected() const {
  uv_run(loop, UV_RUN_NOWAIT);
  return connection_flag;
}
