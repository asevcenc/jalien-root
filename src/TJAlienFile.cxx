/*
 * TJAlienFile.cxx
 *
 *  Created on: Jul 18, 2014
 *      Author: Tatiana Tothova
 */

#include "TJAlienFile.h"
#include "TJAlienResult.h"
#include "TJAlien.h"
#include "TROOT.h"
#include "TObjString.h"
#include "TMap.h"
#include "TObjArray.h"
#include "TString.h"
#include "Rtypes.h"
#include "TSystem.h"
#include "TStopwatch.h"
#include "TVirtualMonitoring.h"
#include "TVirtualMutex.h"
#include "TProcessUUID.h"
#include "TUrl.h"
#include "TError.h"
#include <cstdlib>
#include <regex>
#include <memory>  // unique_ptr

ClassImp(TJAlienFile)

TJAlienFile::TJAlienFile(const char* purl,
                         Option_t* option,
                         const char* ftitle,
                         Int_t compress,
                         Bool_t parallelopen,
                         const char* lurl,
                         const char* authz) :
TAliceFile(purl, option, ftitle, compress, parallelopen, lurl, authz) {
  TUrl logicalUrl(lurl);
  fLfn = logicalUrl.GetFile();
  fAuthz = authz;
  fGUID = "";
  fUrl = "";
  fPfn = "";
  fSE = "";
  fImage = 0;
  fNreplicas = 0;
  fOpenedAt = time(nullptr);
  fElapsed = 0;
}

//______________________________________________________________________________
TJAlienFile*
  TJAlienFile::Open(const char* url, Option_t* option, const char* ftitle, Int_t compress, Bool_t parallelopen) {
  // Open a JAliEn remote file
  // @url JAliEn file path : alien://alice/test/test?<url_options>
  // @option READ/NEW/CREATE/RECREATE/UPDATE : UPDATE=NEW=RECREATE
  // A file can't be updated. If the content of a file is changed a new file is
  // created with the new content and the old one is deleted
  // - Ask for a list of envelopes
  // - Iterate through them until operation is successful or fail after trying
  // all envelopes

  if (gDebug > 1) {
    ::Info("TJAlienFile::Open", "Trying to open file with URL = \"%s\"", url);
  }

  // Check if JAliEn connection exists.
  if (!gGrid) {
    ::Error("TJAlienFile::Open", "No JAliEn Grid connection available!");
    return nullptr;
  }

  // Check if the connection is actually a JAliEn connection
  if ((TString(gGrid->GetGrid())).CompareTo(TString("alien"))) {
    ::Error("TJAlienFile::Open", "You don't have an active JAliEn grid connection!");
    return nullptr;
  }

  // Report this open phase as a temp one, since we have no object yet
  // To extra check, normally it should send a pointer to the current file
  // The information is just stored, not printed because of kFALSE
  if (gMonitoringWriter) {
    gMonitoringWriter->SendFileOpenProgress(nullptr, nullptr, "jalienopen", kFALSE);
  }

  // ----------------------------------------------------------------------------------------
  // Parse input values - url, options
  //

  // Read the filename from the url
  TUrl inputUrl(url);
  TString fJName(inputUrl.GetFile());
  if (gDebug > 1) {
    ::Info("TJAlienFile::Open", "File name: \"%s\"", fJName.Data());
  }

  // Read options
  TString fJOption = option;
  fJOption.ToUpper();

  // There can be options appended to the url
  // E.g.: <url>?se=ALICE::CERN::EOS,!ALICE::SUBATECH::EOS,disk:2&publicaccess=1
  TString inputUrlOptions = inputUrl.GetOptions();
  if (gDebug > 1) {
    ::Info("TJAlienFile::Open", "File URL options: \"%s\"", inputUrlOptions.Data());
  }

  // Maybe there are SE options set in the environment
  TString storageElement = gSystem->Getenv("alien_CLOSE_SE");

  std::unique_ptr<TMap> tags(new TMap());
  tags->SetOwnerKeyValue(kTRUE, kTRUE);
  Bool_t publicAccess = kFALSE;

  // Check if the URL options contain the storage element and the public access info
  std::unique_ptr<TObjArray> objOptions(inputUrlOptions.Tokenize("&"));
  for (Int_t i = 0; i < objOptions->GetEntries(); i++) {
    TString loption = dynamic_cast<TObjString*>(objOptions->At(i))->GetName();
    if (gDebug > 1) {
      ::Info("TJAlienFile::Open", "Option \"%s\"", loption.Data());
    }

    std::unique_ptr<TObjArray> objTags(loption.Tokenize("="));
    if (objTags->GetEntries() == 2) {
      TString key = dynamic_cast<TObjString*>(objTags->At(0))->GetName();
      TString value = dynamic_cast<TObjString*>(objTags->At(1))->GetName();
      if (gDebug > 1) {
        ::Info("TJAlienFile::Open", R"(Option "%s" = "%s")", key.Data(), value.Data());
      }

      // Parse SE information
      if (!key.CompareTo("se", TString::kIgnoreCase)) {
        storageElement = value;

        // Parse tags information (e.g., disk:2,tape:3)
        std::unique_ptr<TObjArray> objSETags(storageElement.Tokenize(","));
        for (Int_t j = 0; j < objSETags->GetEntries(); j++) {
          TString spec = dynamic_cast<TObjString*>(objSETags->At(j))->GetName();
          if (!spec.Contains("::") && spec.Contains(":")) {
            TString tagKey = TString(spec(0, spec.Index(":")));
            TString tagValue = TString(spec(spec.Index(":") + 1, spec.Length()));
            tags->Add(new TObjString(tagKey), new TObjString(tagValue));
            if (gDebug > 1) {
              ::Info("TJAlienFile::Open", R"(Tag "%s" = "%s")", tagKey.Data(), tagValue.Data());
            }
          }
        }
      }

      // Parse publicaccess option
      if (!key.CompareTo("publicaccess", TString::kIgnoreCase)) {
        if (value.Atoi()) {
          publicAccess = kTRUE;
        }
      }
    }
  }

  // If tags map is empty, add "disk:3" and force file to have 3 replicas
  TIter tagIterator(tags->GetTable());
  if (dynamic_cast<TPair*>(tagIterator()) == nullptr) {
    tags->Add(new TObjString("disk"), new TObjString("3"));
    if (storageElement != "") {
      storageElement += ",";
    }

    storageElement += "disk:3";
  }

  // TODO test 'access -p read' (means create file with public access in ROOT,
  // try to READ with this in url: 'publicAccess=1' and test TJAlienFile)

  // ----------------------------------------------------------------------------------------
  // Send command
  //

  Bool_t fJWritable = kFALSE;
  TObject* object = nullptr;
  std::unique_ptr<TListIter> iter;
  std::unique_ptr<TJAlienResult> result;
  int imagenr = 0;  // image number

  // Iterate through the envelopes
  while (true) {
    imagenr++;

    // If out of envelopes, request a new one
    if (!object) {
      // ----------------------------------------------------------------------------------------
      // Construct the command to be sent
      //

      TString command("access");
      if (gDebug > 1) {
        ::Info("TJAlienFile::Open", "Running access command =\"%s\"", command.Data());
      }

      if (fJOption == "UPDATE" || fJOption == "RECREATE") {
        command += (" " + TString("write-version"));
        fJWritable = kTRUE;
      }
      else {
        if (fJOption == "NEW" || fJOption == "CREATE") {
          command += (" " + TString("write-once"));
          fJWritable = kTRUE;
        }
        else {
          // if "READ" or nothing from previous matches - read is default
          if (fJOption == "READ" && publicAccess) {
            command += (" " + TString("-p") + " " + TString("read"));
          }
          else {
            command += (" " + TString("read"));
          }
        }
      }

      command += (" " + fJName);

      if (storageElement != "") {
        command += " ";
        command += storageElement;
      }

      std::unique_ptr<TJAlienResult> tmp(dynamic_cast<TJAlienResult*>(gGrid->Command(command.Data())));
      result.swap(tmp);
      if (!result) {
        ::Error("TJAlienFile::Open", "Failed to get a response from the server");
        return nullptr;
      }

      // Check exit code and result variables
      std::string errorMessage;
      Int_t exitcode = dynamic_cast<TJAlien*>(gGrid)->GetExitCode(result, errorMessage);

      if (exitcode != 0) {
        ::Error("Open", "%s", errorMessage.c_str());
        return nullptr;
      }

      auto list = dynamic_cast<TList*>(result.get());

      if (!list) {
        ::Error("TJAlienFile::Open", "Cannot get the access envelope for %s", fJName.Data());
        // Reset the temp monitoring info
        if (gMonitoringWriter) {
          gMonitoringWriter->SendFileOpenProgress(nullptr, nullptr, nullptr, kFALSE);
        }

        return nullptr;
      }
      if (list->IsEmpty()) {
        if (!fJWritable) {
          ::Error("TJAlienFile::Open", "The file %s does not exist", fJName.Data());
        }
        else {
          ::Error("TJAlienFile::Open", "You don't have rights to open %s", fJName.Data());
        }
        return nullptr;
      }

      // MakeIterator returns a pointer to dynamic mem on heap (we need to clean this up --> done via unique_ptr)
      std::unique_ptr<TListIter> tmpIter(dynamic_cast<TListIter*>(list->MakeIterator()));
      iter.swap(tmpIter);
      object = iter->Next();
    }

    // ----------------------------------------------------------------------------------------
    // Parse response
    //

    Int_t nreplicas = 0;
    TString urlStr, seStr, sguid, pfnStr, authz, tagsStr;

    auto map = dynamic_cast<TMap*>(object);
    const TObject* valueObject;  // Temp object to read values from map

    valueObject = map->GetValue("url");
    if (valueObject) {
      urlStr = valueObject->GetName();
    }

    valueObject = map->GetValue("se");
    if (valueObject) {
      seStr = valueObject->GetName();
    }

    valueObject = map->GetValue("nSEs");
    if (valueObject) {
      nreplicas = ((TString) valueObject->GetName()).Atoi();
    }

    valueObject = map->GetValue("guid");
    if (valueObject) {
      sguid = valueObject->GetName();
    }

    valueObject = map->GetValue("envelope");
    if (valueObject) {
      authz = valueObject->GetName();
    }

    valueObject = map->GetValue("tags");
    if (valueObject) {
      tagsStr = valueObject->GetName();
    }

    if (gDebug > 1) {
      ::Info("TJAlienFile::Open", "%s", urlStr.Data());
      ::Info("TJAlienFile::Open", "%s", seStr.Data());
      ::Info("TJAlienFile::Open", "%d", nreplicas);
      ::Info("TJAlienFile::Open", "%s", sguid.Data());
      ::Info("TJAlienFile::Open", "%s", tagsStr.Data());
    }

    // If envelope does not contain necessary information
    if ((!urlStr) || (!authz)) {
      if (fJWritable) {
        ::Error("TJAlienFile::Open", "Didn't get the authorization to write %s", fJName.Data());
      }
      else {
        ::Error("TJAlienFile::Open", "Didn't get the authorization to read %s from location %s", fJName.Data(),
                seStr.Data());
      }
      gGrid->Stdout();
      gGrid->Stderr();

      // Ban this SE
      if (storageElement != "") {
        storageElement += ",";
      }
      storageElement += "!";
      storageElement += seStr;

      // Try next location
      object = iter->Next();
      continue;
    }

    // ----------------------------------------------------------------------------------------
    // Form physical and logical URLs
    //

    // Extract the anchor from input url
    // alien:///alice/sim/2017/LHC17j8c/245353/001/aod_archive.zip#AliAOD.root
    //                                                             ^  anchor ^
    TString anchor = inputUrl.GetAnchor();

    // If no anchor was specified there, try to get it from server reply
    // root://grid03.hepl.hiroshima-u.ac.jp:1094//07/43168/80a25954-bfdb-11e7-9da3-bbe5aa2e0c61#AliAOD.root
    //                                                                                          ^  anchor ^
    if (anchor.Length() == 0) {
      std::unique_ptr<TObjArray> tokens(urlStr.Tokenize("#"));
      if (tokens->GetEntries() == 2) {
        anchor = dynamic_cast<TObjString*>(tokens->At(1))->GetName();
        urlStr = dynamic_cast<TObjString*>(tokens->At(0))->GetName();
      }
    }

    TUrl logicalUrl(url);
    TUrl physicalUrl(urlStr + TString("?&authz=") + authz);

    if (anchor.Length()) {
      TString purl = physicalUrl.GetUrl();
      purl += "#";
      purl += anchor;
      physicalUrl.SetUrl(purl);
      TString lUrlOpt;
      lUrlOpt = "zip=";
      lUrlOpt += anchor;
      lUrlOpt += "&mkpath=1";
      logicalUrl.SetOptions(lUrlOpt);
      if (gDebug > 1) {
        ::Info("TJAlienFile::Open", "%s", logicalUrl.GetUrl());
        ::Info("TJAlienFile::Open", "%s", logicalUrl.GetFileAndOptions());
      }
    }
    else {
      TString lUrlOpt = logicalUrl.GetOptions();
      if (lUrlOpt.Length()) {
        lUrlOpt += "&mkpath=1";
        logicalUrl.SetOptions(lUrlOpt.Data());
      }
      else {
        logicalUrl.SetOptions("mkpath=1");
      }
    }

    // Add the original options from the alien URL
    if (inputUrlOptions) {
      physicalUrl.SetOptions(physicalUrl.GetOptions() + TString("&") + inputUrlOptions);
    }

    if (gDebug > 1) {
      ::Info("TJAlienFile::Open", "logicalUrl %s", logicalUrl.GetUrl());
      ::Info("TJAlienFile::Open", "physicalUrl %s", physicalUrl.GetUrl());
    }

    // ----------------------------------------------------------------------------------------
    // Actually open file
    //

    if (!fJWritable) {
      if (seStr != "") {
        ::Info("TJAlienFile::Open", "Accessing file %s in SE <%s>", fJName.Data(), seStr.Data());
      }
      else {
        ::Info("TJAlienFile::Open", "Accessing file %s", fJName.Data());
      }
    }

    TStopwatch timer;
    timer.Start();

    auto jalienfile =
      new TJAlienFile(physicalUrl.GetUrl(), fJOption, ftitle, compress, parallelopen, logicalUrl.GetUrl(), authz);
    timer.Stop();

    // ----------------------------------------------------------------------------------------
    // Check its status
    //

    if (jalienfile->IsZombie()) {
      // Bad status

      // Ban this SE
      if (storageElement != "") {
        storageElement += ",";
      }
      storageElement += "!";
      storageElement += seStr;

      // Try next location
      object = iter->Next();
      continue;
    }
    else {
      // Everything is OK, return handler
      jalienfile->SetSE(seStr);

      std::regex reg1("[a-z]+://[a-z0-9-.]+:[0-9]+/");
      jalienfile->SetPfn(std::regex_replace(urlStr.Data(), reg1, "") + "#" + anchor);
      jalienfile->SetImage(imagenr);
      jalienfile->SetNreplicas(nreplicas);
      jalienfile->SetGUID(sguid);
      jalienfile->SetUrl(urlStr + "#" + anchor);
      jalienfile->SetElapsed(timer.RealTime());

      // We successfully wrote the file to the SE
      // So we need to write another n-1 replicas
      // Find a tag, that corresponds to this SE, and decrement its value
      tagIterator = tags->GetTable();
      while (auto pair = dynamic_cast<TPair*>(tagIterator())) {
        auto keyStr = dynamic_cast<TObjString*>(pair->Key());
        auto valueStr = dynamic_cast<TObjString*>(pair->Value());
        TString value = valueStr->GetName();
        int valueInt = value.Atoi();

        if (keyStr && valueStr) {
          if (storageElement.Contains(keyStr->GetName())) {
            TString toReplace(keyStr->GetName());
            toReplace += ":";
            toReplace += valueStr->GetName();
            TString replaceWith(keyStr->GetName());
            replaceWith += ":";
            replaceWith += valueInt - 1;

            storageElement.ReplaceAll(toReplace, replaceWith);
            break;
          }
        }
      }

      jalienfile->SetPreferredSE(storageElement);

      return jalienfile;
    }

    object = iter->Next();
  }

  // ----------------------------------------------------------------------------------------
  // Access failed
  //

  if (fJWritable) {
    if (fJOption == "NEW" || fJOption == "CREATE") {
      ::Error("TJAlienFile::Open", "Could not create file %s", fJName.Data());
    }
    else {
      ::Error("TJAlienFile::Open", "Could not open file %s", fJName.Data());
    }
  }
  else {
    ::Error("TJAlienFile::Open", "Could not open any of the file images of %s", fJName.Data());
  }

  // Reset the temp monitoring info
  if (gMonitoringWriter) {
    gMonitoringWriter->SendFileOpenProgress(nullptr, nullptr, nullptr, kFALSE);
  }

  return nullptr;
}

//______________________________________________________________________________
TJAlienFile::~TJAlienFile() {
  // TJAlienFile file dtor.

  if (IsOpen()) {
    Close();
  }
  if (gDebug) {
    ::Info("~TJAlienFile", "dtor called for %s", GetName());
  }
}

//______________________________________________________________________________
void TJAlienFile::Close(Option_t* option) {
  if (!IsOpen()) {
    return;
  }

  // If READ operation we can exit
  if (fOption == "READ") {
    return;
  }

  // If WRITE operation we need to commit the LFN and PFN to the catalogue
  TString command("commit");

  TAliceFile::Flush();
  Long64_t siz = GetSize();

  // Close file
  TAliceFile::Close(option);

  // On some SEs such as CEPH, stat size is not up-to-date before close
  // With TXNet, GetSize() after close returns fBytesWrite
  // With TNetXNG, GetSize() doesn't work after close
  // Proceed with file commit if internal counters are positive
  if (siz <= 0 && fBytesWrite > 0) {
    if (gDebug > 0) {
      ::Info("TJAlienFile::Close", "XrdStat reported size %lld, but client write counters show %lld bytes", siz,
             fBytesWrite);
    }

    siz = fBytesWrite;
  }

  if (siz <= 0) {
    ::Error("Close", "the reported size of the written file is <= 0");
    return;
  }

  command += " \"";
  command += fAuthz;
  command += "\" ";
  command += siz;
  command += " ";
  command += fLfn;

  std::unique_ptr<TJAlienResult> result(dynamic_cast<TJAlienResult*>(gGrid->Command(command.Data())));
  std::string errorMessage;
  Int_t exitcode = dynamic_cast<TJAlien*>(gGrid)->GetExitCode(result, errorMessage);
  if (exitcode != 0) {
    ::Error("Close", "%s", errorMessage.c_str());
    return;
  }

  if (result->GetKey(0, "lfn") != nullptr) {
    // The file has been committed
    // Create replicas
    Mirror();
  }
  else {
    ::Error("Close", "cannot register %s!", fLfn.Data());
    gSystem->Unlink(fLfn);
  }
}

//______________________________________________________________________________
void TJAlienFile::Mirror() {
  // Send mirror command to create replicas
  TString command("mirror");
  command += " ";
  command += fLfn;
  command += " ";

  // Number of replicas specified at the end of SE list
  command += "-S";
  command += " ";
  command += fPrefSE;

  std::unique_ptr<TJAlienResult> result(dynamic_cast<TJAlienResult*>(gGrid->Command(command.Data())));
  std::string errorMessage;
  Int_t exitcode = dynamic_cast<TJAlien*>(gGrid)->GetExitCode(result, errorMessage);
  if (exitcode != 0) {
    ::Error("Close", "%s", errorMessage.c_str());
  }
}

//______________________________________________________________________________
TString TJAlienFile::SUrl(const char* lfn) {
  // Get surl from lfn by asking AliEn catalog.

  TString command;
  TString surl;
  TString ch(CMDOPTIONS);

  if (!lfn) {
    return surl;
  }

  TUrl lurl(lfn);
  command = TString("access") + ch + TString("read") + ch + lurl.GetFile();

  TGridResult* result;

  if (!gGrid) {
    ::Error("TJAlienFile::SUrl", "no grid connection");
    return surl;
  }

  result = gGrid->Command(command.Data(), kFALSE, TJAlien::kOUTPUT);
  if (!result) {
    ::Error("TJAlienFile::SUrl", "couldn't get access URL for jalien file %s", lfn);
    return surl;
  }

  auto iter = std::unique_ptr<TIterator>(result->MakeIterator());

  if (auto object = iter->Next()) {
    auto map = dynamic_cast<TMap*>(object);
    auto urlObject = map->GetValue("url");
    auto urlStr = dynamic_cast<TObjString*>(urlObject);

    if (urlStr) {
      surl = urlStr->GetName();
      return surl;
    }
  }

  ::Error("TJAlienFile::SUrl", "couldn't get surl for jalien file %s", lfn);
  return surl;
}
