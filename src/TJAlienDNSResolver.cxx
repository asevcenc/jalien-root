#include <iostream>
#include <memory>
#include "TJAlienDNSResolver.h"

using std::random_device;

TJAlienDNSResolver::TJAlienDNSResolver(const string& host, int port) {
  this->host = host;
  this->port = std::to_string(port);
  use_ipv6 = true;
  current_position = 0;
  reset();
}

string TJAlienDNSResolver::addr2string(const struct addrinfo* ai) {
  char dst[32];
  const struct sockaddr* sa = ai->ai_addr;

  if (sa->sa_family == AF_INET6) {
    const auto sin = (const sockaddr_in6*) ai->ai_addr;
    inet_ntop(sa->sa_family, &sin->sin6_addr, dst, ai->ai_addrlen);
  }
  else if (sa->sa_family == AF_INET) {
    const auto sin = (const sockaddr_in*) ai->ai_addr;
    inet_ntop(sa->sa_family, &sin->sin_addr, dst, ai->ai_addrlen);
  }

  return {dst};
}

vector<string> TJAlienDNSResolver::get_addr(const string& resolveHost, const string& resolvePort, int ipv) {
  vector<string> retval;

  auto hints = std::make_unique<addrinfo>();
  hints->ai_socktype = SOCK_STREAM;

  if (ipv == 4) {
    hints->ai_family = AF_INET;
  }
  else if (ipv == 6) {
    hints->ai_family = AF_INET6;
  }
  else {
    hints->ai_family = 0;
  }

  struct addrinfo* ai = nullptr;

  int err = getaddrinfo(resolveHost.c_str(), resolvePort.c_str(), hints.get(), &ai);
  if (err != 0) {
    std::cerr << "Error resolving hostname: " << gai_strerror(err) << std::endl;
    return retval;
  }

  for (struct addrinfo* ri = ai; ri != nullptr; ri = ri->ai_next) {
    retval.push_back(addr2string(ri));
  }

  freeaddrinfo(ai);

  if (!retval.empty()) {
    random_device rng;
    shuffle(begin(retval), end(retval), rng);
  }

  return retval;
}

void TJAlienDNSResolver::reset() {
  addr_ipv4 = get_addr(host, port, 4);
  addr_combined = addr_ipv4;

  if (use_ipv6) {
    addr_ipv6 = get_addr(host.c_str(), port.c_str(), 6);
    addr_combined.insert(addr_combined.end(), addr_ipv6.begin(), addr_ipv6.end());
  }
}

string TJAlienDNSResolver::get_next_host() {
  string result;

  if (current_position >= addr_combined.size()) {
    current_position = 0;
  }

  return addr_combined[current_position++];
}

unsigned long TJAlienDNSResolver::length() {
  return addr_ipv4.size() + addr_ipv6.size();
}
