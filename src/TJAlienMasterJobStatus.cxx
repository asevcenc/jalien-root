// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus   06/10/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienMasterJobStatus                                                //
//                                                                      //
// Status of a MasterJob                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TJAlienJobStatus.h"
#include "TJAlienMasterJobStatus.h"
#include "TObjString.h"
#include "TBrowser.h"

ClassImp(TJAlienMasterJobStatus)

//______________________________________________________________________________
TJAlienMasterJobStatus::~TJAlienMasterJobStatus() {
  // Cleanup.

  fJobs.DeleteAll();

  delete fMasterJob;
}

//______________________________________________________________________________
void TJAlienMasterJobStatus::Browse(TBrowser* b) {
  // Browser interface.

  if (b) {
    auto iter = std::unique_ptr<TIterator>(fJobs.MakeIterator());

    while (auto obj = iter->Next()) {
      auto keyStr = dynamic_cast<TObjString*>(obj);
      auto value = fJobs.GetValue(obj);

      if (keyStr && value) {
        b->Add(value, keyStr->GetString().Data());
      }
    }
  }
}

//______________________________________________________________________________
TGridJobStatus::EGridJobStatus TJAlienMasterJobStatus::GetStatus() const {
  // Returns the status of the master job reduced to the subset defined
  // in TGridJobStatus.

  if (!fMasterJob) {
    return kUNKNOWN;
  }

  return fMasterJob->GetStatus();
}

//______________________________________________________________________________
Float_t TJAlienMasterJobStatus::PercentFinished() {
  // Returns the percentage of finished subjobs, only DONE is considered
  // as finished.

  if (fJobs.GetSize() == 0) {
    return 0;
  }

  auto iter = std::unique_ptr<TIterator>(fJobs.MakeIterator());

  Int_t done = 0;

  while (auto obj = iter->Next()) {
    auto value = fJobs.GetValue(obj);
    auto jobStatus = dynamic_cast<TJAlienJobStatus*>(value);

    if (jobStatus) {
      if (jobStatus->GetStatus() == kDONE) {
        ++done;
      }
    }
  }

  return static_cast<Float_t>(done) / static_cast<Float_t>(fJobs.GetSize());
}

//______________________________________________________________________________
void TJAlienMasterJobStatus::Print(Option_t*) const {
  // Prints information of the master job and the sub job. Only the status is printed.

  if (fMasterJob) {
    printf("Printing information for the master job: ");
    fMasterJob->PrintJob(kFALSE);
  }

  auto iter = std::unique_ptr<TIterator>(fJobs.MakeIterator());

  while (auto obj = iter->Next()) {
    auto keyStr = dynamic_cast<TObjString*>(obj);

    auto value = fJobs.GetValue(obj);
    auto jobStatus = dynamic_cast<TJAlienJobStatus*>(value);

    if (keyStr && jobStatus) {
      printf("Printing info for subjob %s: ", keyStr->GetString().Data());
      jobStatus->PrintJob(kFALSE);
    }
  }
}
