#include "TJClientFile.h"
#include <fstream>
#include <sstream>
#include "TError.h"
#include "TObjArray.h"
#include "TString.h"

TJClientFile::TJClientFile(const char* filepath) {
  tmpdir = getTmpdir();
  fPort = 0;
  fWSPort = 0;

  if (filepath != nullptr) {
    this->isValid = loadFile(filepath);
  }
  else {
    this->isValid = loadFile(getDefaultPath());
  }
}

TString TJClientFile::getDefaultPath() const {
  return tmpdir + TString("/jclient_token_") + TString::Itoa(static_cast<Int_t>(getuid()), 10);
}

TString TJClientFile::getTmpdir() {
  TString tempdir;

  if (getenv("TMPDIR") != nullptr) {
    tempdir = getenv("TMPDIR");
  }
  else if (getenv("TMP") != nullptr) {
    tempdir = getenv("TMP");
  }
  else if (getenv("TEMP") != nullptr) {
    tempdir = getenv("TEMP");
  }
  else {
    tempdir = P_tmpdir;
  }

  return tempdir;
}

bool TJClientFile::loadFile(const char* filepath) {
  if (filepath == nullptr) {
    return false;
  }

  std::ifstream jclientFile(filepath);
  std::string fileLine;

  bool result = true;

  if (jclientFile.is_open()) {
    while (getline(jclientFile, fileLine)) {
      if (gDebug > 1) {
        Info("TJAlien", "Token file line: %s", fileLine.c_str());
      }

      std::istringstream iss(fileLine);
      std::vector<std::string> tokens;
      std::string token;
      while(std::getline(iss, token, '=')) {
        tokens.push_back(token);
      }

      if (tokens.size() == 2) {
        std::string sKey = tokens[0];
        std::string sValue = tokens[1];

        if (gDebug > 1) {
          Info("TJAlien", R"("%s" = "%s")", sKey.c_str(), sValue.c_str());
        }

        if (sKey == "JALIEN_HOST") {
          fHost = sValue;

          if (fHost.Length() == 0) {
            Error("TJAlien", "JAliEn connection host field empty");
            result = false;
            break;
          }
        }

        if (sKey == "JALIEN_PORT") {
          fPort = std::stoi(sValue);
        }

        if (sKey == "JALIEN_WSPORT") {
          fWSPort = std::stoi(sValue);

          if (fWSPort == 0) {
            Error("TJAlien", "JAliEn connection port field empty or misspelled");
            result = false;
            break;
          }
        }

        if (sKey == "JALIEN_HOME") {
          fHome = sValue;
        }

        if (sKey == "JALIEN_USER") {
          fUser = sValue;
        }

        if (sKey == "JALIEN_PASSWORD") {
          fPw = sValue;
        }
      }
      else {
        if (gDebug > 1) {
          Error("TJAlien",
                "jclient file does not have "
                "the correct structure (%lu)",
                tokens.size());
        }
        result = false;
      }
    }

    jclientFile.close();
  }
  else {
    if (gDebug > 1) {
      Error("TJAlien", "Error while opening jclient file");
    }
    result = false;
  }

  return result;
}
