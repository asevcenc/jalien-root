// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus   27/10/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienMasterJob                                                      //
//                                                                      //
// Special Grid job which contains a master job which controls          //
// underlying jobs resulting from job splitting.                        //
//                                                                      //
// Related classes are TJAlienJobStatus.                                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TJAlienJobStatus.h"
#include "TJAlienJobStatusList.h"
#include "TJAlienMasterJob.h"
#include "TJAlienMasterJobStatus.h"
#include "TJAlienJob.h"
#include "TObjString.h"
#include "Riostream.h"
#include "TGridResult.h"
#include "TJAlien.h"
#include "TFileMerger.h"
#include "TBrowser.h"

ClassImp(TJAlienMasterJob)

//______________________________________________________________________________
void TJAlienMasterJob::Browse(TBrowser* b) {
  // Browser interface.

  if (b) {
    b->Add(GetJobStatus());
  }
}

//______________________________________________________________________________
TGridJobStatus* TJAlienMasterJob::GetJobStatus() const {
  // Gets the status of the master job and all its sub jobs.
  // Returns a TJAlienMasterJobStatus object, 0 on failure.

  TString jobID;
  jobID = fJobID;

  TString options = TString("- % - - ");
  options += jobID.Data();
  options += "- - - -";

  auto joblist = dynamic_cast<TJAlienJobStatusList*>(gGrid->Ps(options.Data()));
  auto list = dynamic_cast<TList*>(joblist);
  auto it = std::unique_ptr<TIterator>(list->MakeIterator());

  if (!joblist) {
    return nullptr;
  }

  auto status = std::make_unique<TJAlienMasterJobStatus>(fJobID);

  TJAlienJob masterJob(fJobID);
  status->fMasterJob = dynamic_cast<TJAlienJobStatus*>(masterJob.GetJobStatus());

  while (auto object = it->Next()) {
    auto statusmap = dynamic_cast<TMap*>(object);
    auto jobStatus = std::make_unique<TJAlienJobStatus>(statusmap);
    auto jID = dynamic_cast<TObjString*>(statusmap->GetValue("queueId"));

    if (jID != nullptr) {
      status->fJobs.Add(jID, dynamic_cast<TObject*>(jobStatus.get()));
    }
  }
  return status.release();
}

//______________________________________________________________________________
void TJAlienMasterJob::Print(Option_t* options) const {
  std::cout << " ------------------------------------------------ " << std::endl;
  std::cout << " Master Job ID                   : " << fJobID << std::endl;
  std::cout << " ------------------------------------------------ " << std::endl;
  auto status = std::unique_ptr<TJAlienMasterJobStatus>(dynamic_cast<TJAlienMasterJobStatus*>(GetJobStatus()));
  if (!status) {
    Error("Print", "Cannot get the information for this masterjob");
    return;
  }

  std::cout << " N of Subjobs                    : " << status->GetNSubJobs() << std::endl;
  std::cout << " % finished                      : " << status->PercentFinished() * 100 << std::endl;
  std::cout << " ------------------------------------------------ " << std::endl;
  auto iter = status->GetJobs()->MakeIterator();

  while (auto obj = (TObjString*) iter->Next()) {
    auto substatus = dynamic_cast<TJAlienJobStatus*>(status->GetJobs()->GetValue(obj->GetName()));
    printf(" SubJob: [%-7s] %-10s %20s@%s  RunTime: %s\n", substatus->GetKey("queueId"), substatus->GetKey("status"),
           substatus->GetKey("node"), substatus->GetKey("site"), substatus->GetKey("runtime"));
  }
  std::cout << " ------------------------------------------------ " << std::endl;
  iter->Reset();
  if (strchr(options, 'l')) {
    while (auto obj = (TObjString*) iter->Next()) {
      auto substatus = dynamic_cast<TJAlienJobStatus*>(status->GetJobs()->GetValue(obj->GetName()));
      // list sandboxes
      auto outputdir = substatus->GetJdlKey("OutputDir");

      TString sandbox;
      if (outputdir) {
        sandbox = outputdir;
      }
      else {
        sandbox = TString("/proc/") + TString(substatus->GetKey("user")) + TString("/") +
                  TString(substatus->GetKey("queueId")) + TString("/job-output");
      }

      printf(" Sandbox [%-7s] %s \n", substatus->GetKey("queueId"), sandbox.Data());
      std::cout << " ================================================ " << std::endl;

      if (!gGrid->Cd(sandbox)) {
        continue;
      }

      auto dirlist = std::unique_ptr<TGridResult>(gGrid->Ls(sandbox));
      dirlist->Sort(kTRUE);
      Int_t i = 0;
      while (dirlist->GetFileName(i)) {
        printf("%-24s ", dirlist->GetFileName(i++));
        if (!(i % 4)) {
          printf("\n");
        }
      }
      printf("\n");
    }
  }
  std::cout << " ----------LITE_JOB_OPERATIONS-------------------------------------- " << std::endl;
}

//______________________________________________________________________________
Bool_t TJAlienMasterJob::Merge() {
  return kFALSE;
}

//______________________________________________________________________________
Bool_t TJAlienMasterJob::Merge(const char* inputname, const char* mergeoutput) const {
  TFileMerger merger;

  auto status = std::unique_ptr<TJAlienMasterJobStatus>(dynamic_cast<TJAlienMasterJobStatus*>(GetJobStatus()));
  auto iter = status->GetJobs()->MakeIterator();

  while (auto obj = (TObjString*) iter->Next()) {
    auto substatus = dynamic_cast<TJAlienJobStatus*>(status->GetJobs()->GetValue(obj->GetName()));
    TString sandbox;  // list sandboxes
    auto outputdir = substatus->GetJdlKey("OutputDir");
    printf(" Sandbox [%-7s] %s \n", substatus->GetKey("queueId"), sandbox.Data());
    std::cout << " ================================================ " << std::endl;
    if (outputdir) {
      sandbox = outputdir;
    }
    else {
      sandbox = TString("/proc/") + TString(substatus->GetKey("user")) + TString("/") +
                TString(substatus->GetKey("queueId")) + TString("/job-output");
    }
    merger.AddFile(TString("alien://") + sandbox + TString("/") + TString(inputname));
  }

  if (mergeoutput) {
    merger.OutputFile(mergeoutput);
  }

  return merger.Merge();
}
