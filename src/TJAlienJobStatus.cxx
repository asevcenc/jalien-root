/*
 * TJAlienJobStatus.cxx
 *
 *  Created on: Sep 4, 2014
 *      Author: Tatianka Tothova
 */

#include "TGridJobStatus.h"
#include "TJAlienJobStatus.h"
#include "TObjString.h"
#include "TBrowser.h"
#include "TNamed.h"
#include "TJAlienDirectory.h"

ClassImp(TJAlienJobStatus)

//______________________________________________________________________________
TJAlienJobStatus::TJAlienJobStatus(TMap* status) {
  // Creates a TJAlienJobStatus object.
  // If a status map is provided it is copied to the status information.

  TObjString* key;
  TObjString* val;

  if (status) {
    TMapIter next(status);
    while ((key = (TObjString*) next())) {
      val = (TObjString*) status->GetValue(key->GetName());
      fStatus.Add(key->Clone(), val->Clone());
    }
  }
}

TJAlienJobStatus::~TJAlienJobStatus() {
  // Cleanup.

  fStatus.DeleteAll();
}

//______________________________________________________________________________
void TJAlienJobStatus::Browse(TBrowser* b) {
  // Browser interface to ob status.

  if (b) {
    auto iter = std::unique_ptr<TIterator>(fStatus.MakeIterator());
    while (auto obj = iter->Next()) {
      auto value = std::unique_ptr<TObject>(fStatus.GetValue(obj));

      auto keyStr = dynamic_cast<TObjString*>(obj);
      auto valueStr = dynamic_cast<TObjString*>(value.get());

      if (keyStr->GetString() == TString("jdl")) {
        TString valueParsed(valueStr->GetString());
        valueParsed.ReplaceAll("\n", 1);
        valueParsed.ReplaceAll("  ", 2);
        b->Add(new TPair(new TObjString("jdl"), new TObjString(valueParsed)));

        // list sandboxes
        auto outputdir = GetJdlKey("OutputDir");

        TString sandbox;
        if (outputdir) {
          sandbox = outputdir;
        }
        else {
          sandbox = TString("/proc/") + TString(GetKey("user")) + TString("/") + TString(GetKey("queueId")) +
                    TString("/job-output");
        }

        b->Add(new TJAlienDirectory(sandbox.Data(), "job-output"));
      }
      else {
        if (keyStr && valueStr) {
          b->Add(new TNamed(valueStr->GetString(), keyStr->GetString()));
        }
      }
    }
  }
}

//______________________________________________________________________________
const char* TJAlienJobStatus::GetJdlKey(const char* key) {
  // Return the JDL key.

  const char* jdl = GetKey("jdl");
  if (!jdl) {
    return nullptr;
  }
  const char* jdltagbegin = strstr(jdl, key);
  const char* jdltagquote = strchr(jdltagbegin, '"');
  const char* jdltagend = strchr(jdltagbegin, ';');

  if (!jdltagend) {
    return nullptr;
  }
  if (!jdltagquote) {
    return nullptr;
  }
  jdltagquote++;
  const char* jdltagquote2 = strchr(jdltagquote, '"');
  if (!jdltagquote2) {
    return nullptr;
  }
  fJdlTag = TString(jdltagquote);
  fJdlTag = fJdlTag(0, jdltagquote2 - jdltagquote);

  return fJdlTag.Data();
}

//______________________________________________________________________________
const char* TJAlienJobStatus::GetKey(const char* key) {
  // Return a key.

  auto obj = fStatus.FindObject(key);
  auto pair = dynamic_cast<TPair*>(obj);
  if (pair) {
    auto string = dynamic_cast<TObjString*>(pair->Value());
    return string->GetName();
  }
  return nullptr;
}

//______________________________________________________________________________
TGridJobStatus::EGridJobStatus TJAlienJobStatus::GetStatus() const {
  // Gets the status of the job reduced to the subset defined
  // in TGridJobStatus.

  auto obj = fStatus.FindObject("status");
  auto pair = dynamic_cast<TPair*>(obj);

  if (pair) {
    auto string = dynamic_cast<TObjString*>(pair->Value());

    if (string) {
      const char* status = string->GetString().Data();

      if (strcmp(status, "INSERTING") == 0 || strcmp(status, "WAITING") == 0 || strcmp(status, "QUEUED") == 0 ||
          strcmp(status, "ASSIGNED") == 0) {
        return kWAITING;
      }
      else if (strcmp(status, "STARTED") == 0 || strcmp(status, "SAVING") == 0 || strcmp(status, "SPLITTING") == 0 ||
               strcmp(status, "RUNNING") == 0 || strcmp(status, "SPLIT") == 0) {
        return kRUNNING;
      }
      else if (strcmp(status, "EXPIRED") == 0 || string->GetString().BeginsWith("ERROR_") ||
               strcmp(status, "FAILED") == 0 || strcmp(status, "ZOMBIE") == 0) {
        return kFAIL;
      }
      else if (strcmp(status, "KILLED") == 0) {
        return kABORTED;
      }
      else if (strcmp(status, "DONE") == 0) {
        return kDONE;
      }
    }
  }
  return kUNKNOWN;
}

//______________________________________________________________________________
void TJAlienJobStatus::Print(Option_t*) const {
  // Prints the job information.

  PrintJob(kTRUE);
}

//______________________________________________________________________________
void TJAlienJobStatus::PrintJob(Bool_t full) const {
  // Prints this job.
  // If full is kFALSE only the status is printed, otherwise all information.

  auto obj = fStatus.FindObject("status");
  auto pair = dynamic_cast<TPair*>(obj);

  if (pair) {
    auto string = dynamic_cast<TObjString*>(pair->Value());
    if (string) {
      printf("The status of the job is %s\n", string->GetString().Data());
    }
  }

  if (!full) {
    return;
  }

  printf("==================================================\n");
  printf("Detail Information:\n");

  auto iter = std::unique_ptr<TIterator>(fStatus.MakeIterator());

  while (auto object = iter->Next()) {
    auto value = fStatus.GetValue(object);

    auto keyStr = dynamic_cast<TObjString*>(object);
    auto valueStr = dynamic_cast<TObjString*>(value);

    printf("%s => %s\n", (keyStr) ? keyStr->GetString().Data() : "", (valueStr) ? valueStr->GetString().Data() : "");
  }
}
