# Find module for JSON-C.
# Variables:
#  - JSONC_FOUND: TRUE if found
#  - JSONC_LIBPATH: library path
#  - JSONC_INCLUDE_DIR: include dir
include(FindPackageHandleStandardArgs)
find_library(JSONC_LIBPATH "json-c"
        PATH_SUFFIXES "lib"
        HINTS "${JSONC}")
find_path(JSONC_INCLUDE_DIR "json_object_iterator.h"
        PATH_SUFFIXES "include/json-c"
        HINTS "${JSONC}")
get_filename_component(JSONC_LIBPATH ${JSONC_LIBPATH} DIRECTORY)
get_filename_component(JSONC_INCLUDE_DIR ${JSONC_INCLUDE_DIR} DIRECTORY)
find_package_handle_standard_args(JSONC DEFAULT_MSG
        JSONC_LIBPATH JSONC_INCLUDE_DIR)
include_directories(${JSONC_INCLUDE_DIR})
link_directories(${JSONC_LIBPATH})
