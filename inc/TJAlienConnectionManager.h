// Author: Volodymyr Yurchenko 27/06/2019

#ifndef ROOT_TJAlienConnectionManager
#define ROOT_TJAlienConnectionManager

// Needed for compatibility with slc7 and RHEL8 when using ROOT 6.30.xx
#ifdef _TIME_H
#define _STRUCT_TIMESPEC
#endif

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <json-c/json.h>
#else
struct json_object;
#endif

#include <sys/stat.h>
#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <libwebsockets.h>
#include "lws_config.h"
#else
struct lws_context;
struct lws_context_creation_info;
struct lws_retry_bo_t;
#endif

#include "TJAlienResult.h"
#include "TJAlienCredentials.h"
#include "TJAlienDNSResolver.h"
#include "TJClientFile.h"
#include "TError.h"
#include "TGrid.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <fstream>
#include <memory>

#define DEFAULT_JCENTRAL_SERVER "alice-jcentral.cern.ch"
#define UNUSED(x)               (void) (x)

typedef struct uv_loop_s uv_loop_t;

class TJAlienConnectionManager {
private:
  const int default_WSport = 8097;

  const std::string default_server = DEFAULT_JCENTRAL_SERVER;
  std::string fWSHost;  // websocket host
  int fWSPort {};       // websocket port
  TString sUsercert;    // location of user certificate
  TString sUserkey;     // location of user private key

  // Libwebsockets
  static int destroy_flag;  // Flags to know connection status
  static int connection_flag;
  static int writeable_flag;
  static int receive_flag;

  static const int default_ping_interval = 10;  // ping interval
  static const int default_timeout = 20;        // no __network__ __activity__ this interval, let's close/timeout
  static const int lws_ping_interval;
  static const int lws_timeout;

  static const lws_retry_bo_t retry;

  struct lws_context* context {};  // Context contains all information about connection
  struct lws* wsi {};              // WebSocket Instance - real connection object, created basing on context

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
  static int ws_service_callback(  // Callback to handle connection
    struct lws* wsi,
    enum lws_callback_reasons reason,
    void* user,
    void* in,
    size_t len);
  int websocket_write_back(const char* str, int str_size_in);
#endif

  static uv_loop_t* loop;  // libuv event loop used to poll for server replies

  static void clearFlags();
  TJAlienCredentials creds;
  static std::string readBuffer;

public:
  TJAlienConnectionManager() = default;  // default constructor

  ~TJAlienConnectionManager();
  int CreateConnection();
  void ConnectJBox(const TJAlienCredentialsObject& c);
  void ConnectJCentral(const TJAlienCredentialsObject& c, const string& host = DEFAULT_JCENTRAL_SERVER);
  void MakeWebsocketConnection(const TJAlienCredentialsObject& credentialsObject, const string& host, int WSPort);
  void ForceRestart();
  std::unique_ptr<TJAlienResult> RunJsonCommand(const TString& command, const TList* options);
  std::unique_ptr<TJAlienResult> RunJsonCommand(const TString& command,
                                                const TList* options,
                                                std::map<std::string, std::string>& metadata,
                                                std::string& buffer);

  // Parse the result from Json structure
  static std::unique_ptr<TJAlienResult> GetCommandResult(json_object* json_response, bool expand_find = false);

private:
  // Format command to Json structure
  static json_object* CreateJsonCommand(const TString& command, const TList* options);

public:
  Bool_t IsConnected() const;

  ClassDef(TJAlienConnectionManager, 0)
};
#endif
