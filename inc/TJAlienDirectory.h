// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus   28/9/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienDirectory
#define ROOT_TJAlienDirectory

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienDirectory                                                      //
//                                                                      //
// Class which creates Directory files for the AliEn middleware.        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_Tlist
#include "TList.h"
#endif
#ifndef ROOT_TBrowser
#include "TBrowser.h"
#endif
#ifndef ROOT_TNamed
#include "TNamed.h"
#endif
#ifndef ROOT_TMap
#include "TMap.h"
#endif

class TJAlienDirectoryEntry : public TNamed {
private:
  TString fLfn;          // logical file name
  TMap fBrowserObjects;  // objects shown in browser

public:
  TJAlienDirectoryEntry(const char* lfn, const char* name) : TNamed(name, name) { fLfn = lfn; }

  ~TJAlienDirectoryEntry() override = default;

  Bool_t IsFolder() const override { return kTRUE; }

  void Browse(TBrowser* b) override;

  ClassDefOverride(TJAlienDirectoryEntry, 1)  // Creates Directory files entries for the AliEn middleware
};

class TJAlienDirectory : public TNamed {
private:
  TList fEntries;        // directory entries
  TMap fBrowserObjects;  // objects shown in browser

public:
  explicit TJAlienDirectory(const char* ldn, const char* name = nullptr);
  ~TJAlienDirectory() override;
  void Fill();

  Bool_t IsFolder() const override { return kTRUE; }

  void Browse(TBrowser* b) override;

  ClassDefOverride(TJAlienDirectory, 1)  // Creates Directory files for the AliEn middleware
};

#endif
