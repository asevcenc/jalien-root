// @(#)root/alien:$Id$
// Author: Fons Rademakers   3/1/2002

/*************************************************************************
 * Copyright (C) 1995-2002, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienResult
#define ROOT_TJAlienResult

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TAliEnResult                                                     //
//                                                                      //
// Class defining interface to a Alien result set.                      //
// Objects of this class are created by TGrid methods.                  //
//                                                                      //
// Related classes are TAlien.                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridResult
#include "TGridResult.h"
#endif

#ifndef ROOT_TMap
#include <TMap.h>
#endif
#include "TObjString.h"
#define CMDEND '\0'
#include <map>

class TJAlienResult : public TGridResult {
private:
  mutable TString fFilePath;  // file path
  std::map<std::string, std::string> fJAliEnMetaData;

public:
  TJAlienResult();
  ~TJAlienResult() override;

  void DumpResult();
  const char* GetFileName(UInt_t i) const override;              // returns the file name of list item i
  const char* GetFileNamePath(UInt_t i) const override;          // returns the full path + file name of list item i
  const TEntryList* GetEntryList(UInt_t i) const override;       // returns an entry list, if it is defined
  const char* GetPath(UInt_t i) const override;                  // returns the file path of list item i
  const char* GetKey(UInt_t i, const char* key) const override;  // returns the key value of list item i
  Bool_t SetKey(UInt_t i, const char* key, const char* value) override;  // set the key value of list item i
  TList* GetFileInfoList() const override;  // returns a new allocated List of TFileInfo Objects
  using TCollection::Print;
  void Print(Option_t* option = "") const override;

  void Add(TObject*) override;  // special Add function (to perform some checks)

  std::string GetMetaData(const std::string& key) const;
  void SetMetaData(const std::string& key, const std::string& value);

  ClassDefOverride(TJAlienResult, 0)  // Alien query result set
};

#endif
