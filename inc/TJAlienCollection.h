/*
 * TJAlienCollection.h
 *
 *  Created on: Aug 12, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienCollection
#define ROOT_TJAlienCollection

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienCollection                                                     //
//                                                                      //
// Class which manages collection of files on JAliEn middleware.         //
// The file collection is in the form of an XML file.                   //
//                                                                      //
// The internal list is managed as follows:                             //
// TList* ===> TMap*(file) ===> TMap*(attributes)                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TAliceCollection.h"
#include "TString.h"
#include "TList.h"
#include "TFileStager.h"

class TJAlienCollection : public TAliceCollection {
private:
  TString fXmlFile;           // collection XML file
  TList* fFileGroupList;      //-> list with event file maps
  TIter* fFileGroupListIter;  //! event file list iterator
  TMap* fCurrent;             //! current event file map
  UInt_t fNofGroups;          // number of file groups
  UInt_t fNofGroupfiles;      // number of files per group
  Bool_t fHasSUrls;           // defines if SURLs are present in the collection
  Bool_t fHasSelection;       // defines if the user made some selection on the files
                              // to be exported for processing
  Bool_t fHasOnline;          // defines if the collection was checked for the online status
  TString fLastOutFileName;   // keeps the latest outputfilename produced with GetOutputFileName
  TFileStager* fFileStager;   //! pointer to the file stager object
  TString fExportUrl;         // defines the url where to store back this collection
  TString fInfoComment;       // comment in the info section of the XML file
  TString fCollectionName;    // name of the collection in the collection section of the XML file
  TList* fTagFilterList;      //-> list of TObjStrings with tags to filter out in export operations

  virtual void ParseXML(UInt_t maxentries);
  Bool_t ExportXML(TFile* file, Bool_t selected, Bool_t online, const char* name, const char* comment);

public:
  TJAlienCollection() :
  fFileGroupList(nullptr),
  fFileGroupListIter(nullptr),
  fCurrent(nullptr),
  fNofGroups(0),
  fNofGroupfiles(0),
  fHasSUrls(kFALSE),
  fHasSelection(kFALSE),
  fHasOnline(kFALSE),
  fFileStager(nullptr),
  fExportUrl(""),
  fInfoComment(""),
  fCollectionName("unnamed"),
  fTagFilterList(nullptr) {}

  TJAlienCollection(TList* eventlist, UInt_t nofgroups, UInt_t nofgroupfiles);
  TJAlienCollection(const char* localCollectionFile, UInt_t maxentries);
  ~TJAlienCollection() override;

  TFileCollection* GetFileCollection(const char* name = "", const char* title = "") const override;

  void Add(TGridCollection* addcollection) override;
  void AddFast(TGridCollection* addcollection) override;
  Bool_t CheckIfOnline(Bool_t bulk = kFALSE) override;
  Bool_t
    ExportXML(const char* exporturl, Bool_t selected, Bool_t online, const char* name, const char* comment) override;
  TEntryList* GetEntryList(const char* name) override;
  TGridResult*
    GetGridResult(const char* filename = "", Bool_t onlyonline = kTRUE, Bool_t publicaccess = kFALSE) override;
  const char* GetLFN(const char* name = "") override;
  const char* GetOutputFileName(const char* infile, Bool_t rename = kTRUE, const char* suffix = "root");
  Long64_t GetSize(const char* name = "") override;
  const char* GetSURL(const char* name = "") override;
  const char* GetTURL(const char* name = "") override;
  Bool_t IsSelected(const char* name = "") override;
  Bool_t IsOnline(const char* name = "") override;
  Bool_t LookupSUrls(Bool_t verbose = kTRUE) override;
  TMap* Next() override;
  TFile* OpenFile(const char* filename) override;
  Bool_t OverlapCollection(TGridCollection* comparator) override;
  void Print(Option_t* opt) const override;
  void Reset() override;
  Bool_t Remove(TMap* map) override;
  Bool_t SelectFile(const char* name, Int_t /*start*/ = -1, Int_t /*stop*/ = -1) override;
  Bool_t DeselectFile(const char* name, Int_t /*start*/ = -1, Int_t /*stop*/ = -1) override;
  Bool_t DownscaleSelection(UInt_t scaler = 2) override;
  Bool_t InvertSelection() override;
  void SetTag(const char* tag, const char* value, TMap* tagmap) override;
  Bool_t SetExportUrl(const char* exporturl = nullptr) override;
  Bool_t Stage(Bool_t bulk = kFALSE, Option_t* option = "") override;
  void Status() override;

  const char* GetExportUrl() override {
    if (fExportUrl.Length()) {
      return fExportUrl;
    }
    else {
      return nullptr;
    }
  }  // return's (if defined) the export url protected:

  TList* GetFileGroupList() const override { return fFileGroupList; }

  UInt_t GetNofGroups() const override { return fNofGroups; }

  UInt_t GetNofGroupfiles() const override { return fNofGroupfiles; }

  TList* GetTagFilterList() const override { return fTagFilterList; }

  void SetTagFilterList(TList* filterlist) override {
    delete fTagFilterList;
    fTagFilterList = filterlist;
  }

  const char* GetCollectionName() const override { return fCollectionName.Data(); }

  const char* GetInfoComment() const override { return fInfoComment.Data(); }

  Bool_t Prepare(Bool_t bulk = kFALSE) override { return Stage(bulk, "option=0"); }

  static TGridCollection* OpenQuery(const TGridResult* queryresult, Bool_t nogrouping = kFALSE);
  static TGridCollection* Open(const char* collectionurl, UInt_t maxentries = 1000000);
  static TJAlienCollection* OpenJAliEnCollection(const TGridResult* queryresult, Option_t* option = "");

  ClassDefOverride(TJAlienCollection, 1)  // Manages collection of files on JAliEn
};

#endif
