/*
 * TJAlienJobStatus.h
 *
 *  Created on: Sep 4, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienJobStatus
#define ROOT_TJAlienJobStatus

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienJobStatus                                                      //
//                                                                      //
// JAliEn implementation of TGridJobStatus.                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TAliceJobStatus.h"
#include "TMap.h"

class TJAlienJob;
class TJAlienMasterJob;

class TJAlienJobStatus : public TAliceJobStatus {
  friend class TJAlienJob;
  friend class TJAlienMasterJob;

private:
  TMap fStatus;     // Contains the status information of the job.
                    // In the Alien implementation this is a string, string map.
  TString fJdlTag;  // JdlTag

public:
  TJAlienJobStatus() = default;
  explicit TJAlienJobStatus(TMap* status);
  ~TJAlienJobStatus() override;

  const char* GetJdlKey(const char* key) override;
  const char* GetKey(const char* key) override;

  TGridJobStatus::EGridJobStatus GetStatus() const override;
  void Print(Option_t*) const override;
  virtual void PrintJob(Bool_t full) const;

  Bool_t IsFolder() const override { return kTRUE; }

  void Browse(TBrowser* b) override;

  ClassDefOverride(TJAlienJobStatus, 1)  // JAliEn implementation of TGridJobStatus
};

#endif /* ROOT_TJAlienJobStatus */
