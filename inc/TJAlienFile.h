/*
 * TJAlienFile.h
 *
 *  Created on: Jul 18, 2014
 *      Author: Tatiana Tothova
 */

#ifndef ROOT_TJAlienFile
#define ROOT_TJAlienFile

#include "TAliceFile.h"

class TUrl;

class TJAlienFile : public TAliceFile {
private:
  TString fLfn;        // logical file name
  TString fAuthz;      // authorization envelope
  TString fGUID;       // GUID
  TString fUrl;        // url for the opened copy
  TString fPfn;        // physical file name
  TString fSE;         // Storage element
  TString fPrefSE;     // List of preferred storage elements
  Int_t fImage;        // Image number
  Int_t fNreplicas;    // Number of replicas
  Long64_t fOpenedAt;  // Absolute value for time when opened
  Double_t fElapsed;   // Time elapsed to open a file
public:
  TJAlienFile() :
  TAliceFile(),
  fLfn(),
  fAuthz(),
  fGUID(),
  fUrl(),
  fPfn(),
  fSE(),
  fImage(0),
  fNreplicas(0),
  fOpenedAt(0),
  fElapsed(0) {}

  explicit TJAlienFile(const char* purl,
                       Option_t* option = "",
                       const char* ftitle = "",
                       Int_t compress = 1,
                       Bool_t parallelopen = kFALSE,
                       const char* lurl = nullptr,
                       const char* authz = nullptr);
  ~TJAlienFile() override;

  void Close(const Option_t* opt = "") override;
  void Mirror();

  const char* GetGUID() const override { return fGUID; }

  Double_t GetElapsed() const override { return fElapsed; }

  Int_t GetImage() const override { return fImage; }

  const char* GetLfn() const override { return fLfn; }

  Int_t GetNreplicas() const override { return fNreplicas; }

  Long64_t GetOpenTime() const override { return fOpenedAt; }

  const char* GetPfn() const override { return fPfn; }

  const char* GetSE() const override { return fSE; }

  const char* GetUrl() const override { return fUrl; }

protected:
  void SetGUID(const char* guid) override { fGUID = guid; }

  void SetElapsed(Double_t real) override { fElapsed = real; }

  void SetImage(Int_t image) { fImage = image; }

  void SetNreplicas(Int_t nrep) override { fNreplicas = nrep; }

  void SetPfn(const char* pfn) override { fPfn = pfn; }

  void SetSE(const char* se) override { fSE = se; }

  void SetPreferredSE(const char* prefse) override { fPrefSE = prefse; }

  void SetUrl(const char* url) override { fUrl = url; }

public:
  static TJAlienFile* Open(const char* url,
                           const Option_t* option = "",
                           const char* title = "",
                           Int_t compress = 1,
                           Bool_t parallelopen = kFALSE);
  static TString SUrl(const char* lfn);

  ClassDefOverride(TJAlienFile, 4)  //A ROOT file that reads/writes via AliEn services and TXNetFile protocol
};

#endif /* TJALIENFILE_H_ */
