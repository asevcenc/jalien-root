// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus  27/10/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienMasterJob
#define ROOT_TJAlienMasterJob

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienMasterJob                                                      //
//                                                                      //
// Special Grid job which contains a master job which controls          //
// underlying jobs resulting from job splitting.                        //
//                                                                      //
// Related classes are TJAlienJobStatus.                                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridJob
#include "TGridJob.h"
#endif

class TJAlienMasterJob : public TGridJob {
public:
  explicit TJAlienMasterJob(const TString& jobID) : TGridJob(jobID) {}

  ~TJAlienMasterJob() override = default;

  TGridJobStatus* GetJobStatus() const override;

  void Print(Option_t*) const override;
  Bool_t Merge();
  Bool_t Merge(const char* inputname, const char* mergeoutput = nullptr) const;
  void Browse(TBrowser* b) override;

  ClassDefOverride(TJAlienMasterJob, 1)  // Special Alien grid job controlling results of job splitting
};

#endif
