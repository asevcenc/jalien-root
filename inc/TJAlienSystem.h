// @(#)root/base:$Id$
// Author: Andreas Peters  15/05/2006

/*************************************************************************
 * Copyright (C) 1995-2006, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienSystem
#define ROOT_TJAlienSystem

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienSystem                                                         //
//                                                                      //
// TSystem Implementation of the AliEn GRID plugin.                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef __CINT__
#include <cstdio>
#endif

#ifndef ROOT_TSystem
#include "TSystem.h"
#endif

class TJAlienSystem : public TSystem {
public:
  explicit TJAlienSystem(const char* name = "Generic", const char* title = "Generic System");
  ~TJAlienSystem() override;

  //---- Misc
  Bool_t Init() override;

  //---- Directories
  int MakeDirectory(const char* name) override;
  void* OpenDirectory(const char* name) override;
  void FreeDirectory(void* dirp) override;
  const char* GetDirEntry(void* dirp) override;

  void* GetDirPtr() const override { return nullptr; }

  Bool_t ChangeDirectory(const char* path) override;
  const char* WorkingDirectory() override;
  const char* HomeDirectory(const char* userName = nullptr) override;
  int mkdir(const char* name, Bool_t recursive = kFALSE) override;

  //---- Paths & Files
  int CopyFile(const char* from, const char* to, Bool_t overwrite = kFALSE) override;
  int Rename(const char* from, const char* to) override;
  int Link(const char* from, const char* to) override;
  int Symlink(const char* from, const char* to) override;
  int Unlink(const char* name) override;
  int GetPathInfo(const char* path, FileStat_t& buf) override;
  int GetFsInfo(const char* path, Long_t* id, Long_t* bsize, Long_t* blocks, Long_t* bfree) override;
  int Chmod(const char* file, UInt_t mode) override;
  int Umask(Int_t mask) override;
  int Utime(const char* file, Long_t modtime, Long_t actime) override;
  const char* FindFile(const char* search, TString& file, EAccessMode mode = kFileExists) override;
  Bool_t AccessPathName(const char* path, EAccessMode mode = kFileExists) override;

  //---- Users & Groups
  Int_t GetUid(const char* user = nullptr) override;
  Int_t GetGid(const char* group = nullptr) override;
  Int_t GetEffectiveUid() override;
  Int_t GetEffectiveGid() override;
  UserGroup_t* GetUserInfo(Int_t uid) override;
  UserGroup_t* GetUserInfo(const char* user = nullptr) override;
  UserGroup_t* GetGroupInfo(Int_t gid) override;
  UserGroup_t* GetGroupInfo(const char* group = nullptr) override;

  ClassDefOverride(TJAlienSystem, 0)  // System interface to the Alien Catalogue
};

#endif
