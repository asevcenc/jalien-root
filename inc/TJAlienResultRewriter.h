#ifndef ROOT_TJAlienResultRewriter
#define ROOT_TJAlienResultRewriter
#include "TJAlienResult.h"
#include <map>
#include <utility>
#include <string>

class TJAlienResultRewriter {
private:
  static const std::map<std::string, std::pair<std::string, std::string>> rules;
  TMap* ObtainFirstMap(TJAlienResult* result);
  void SetResultCode(TJAlienResult* result, const char* target);
  void RemoveResultCode(TMap* m);

public:
  void Rewrite(const std::string& command, TJAlienResult* inResult);
  void ApplyRule(const std::pair<std::string, std::string>* rule, TJAlienResult* result);

  bool IsSuccess(TJAlienResult* result);
  bool IsFailure(TJAlienResult* result);

  ClassDef(TJAlienResultRewriter, 0);  // Insert return codes like in TAlien
};

#endif
