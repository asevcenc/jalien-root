// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus   28/9/2004
//         Lucia.Jancurova@cern.ch Slovakia 2007
/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienJDL
#define ROOT_TJAlienJDL

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienJDL                                                            //
//                                                                      //
// Class which creates JDL files for the alien middleware.              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridJDL
#include "TGridJDL.h"
#endif

class TJAlienJDL : public TGridJDL {
public:
  TJAlienJDL() : TGridJDL() {}

  ~TJAlienJDL() override = default;

  void SetExecutable(const char* value = nullptr, const char* description = nullptr) override;
  void SetArguments(const char* value = nullptr, const char* description = nullptr) override;
  void SetEMail(const char* value = nullptr, const char* description = nullptr) override;
  void SetOutputDirectory(const char* value = nullptr, const char* description = nullptr) override;
  void SetPrice(UInt_t price = 1, const char* description = nullptr) override;
  void SetMergedOutputDirectory(const char* value = nullptr, const char* description = nullptr);
  void SetTTL(UInt_t ttl = 72000, const char* description = nullptr) override;
  void SetJobTag(const char* jobtag = nullptr, const char* description = nullptr) override;
  void SetInputDataListFormat(const char* format = "xml-single", const char* description = nullptr) override;
  void SetInputDataList(const char* list = "collection.xml", const char* description = nullptr) override;

  void SetSplitMode(const char* value,
                    UInt_t maxnumberofinputfiles = 0,
                    UInt_t maxinputfilesize = 0,
                    const char* d1 = nullptr,
                    const char* d2 = nullptr,
                    const char* d3 = nullptr) override;
  void SetSplitModeMaxNumOfFiles(UInt_t maxnumberofinputfiles = 0, const char* description = nullptr);
  void SetSplitModeMaxInputFileSize(UInt_t maxinputfilesize = 0, const char* description = nullptr);
  void SetSplitArguments(const char* splitarguments = nullptr, const char* description = nullptr) override;
  void SetValidationCommand(const char* value, const char* description = nullptr) override;
  void SetMaxInitFailed(Int_t maxInitFailed, const char* description = nullptr);

  void SetOwnCommand(const char* command = nullptr, const char* value = nullptr, const char* description = nullptr);

  void AddToInputSandbox(const char* value = nullptr, const char* description = nullptr) override;
  void AddToOutputSandbox(const char* value = nullptr, const char* description = nullptr) override;
  void AddToInputData(const char* value = nullptr, const char* description = nullptr) override;
  void AddToInputDataCollection(const char* value = nullptr, const char* description = nullptr) override;
  void AddToRequirements(const char* value = nullptr, const char* description = nullptr) override;
  void AddToPackages(const char* name /*="AliRoot"*/,
                     const char* version /*="newest"*/,
                     const char* type /*="VO_ALICE"*/,
                     const char* description = nullptr) override;
  void AddToPackages(const char* name /*="VO_ALICE@AliRoot::newest"*/, const char* description = nullptr);
  void AddToOutputArchive(const char* value = nullptr, const char* description = nullptr) override;
  void AddToReqSet(const char* key, const char* value = nullptr);

  void AddToMerge(const char* filenameToMerge /*="histograms.root"*/,
                  const char* jdlToSubmit /*="/alice/jdl/mergerootfile.jdl"*/,
                  const char* mergedFile /*="histograms-merged.root"*/,
                  const char* description = nullptr);
  void AddToMerge(const char* merge = "histo.root:/alice/jdl/mergerootfile.jdl:histo-merged.root",
                  const char* description = nullptr);

  void SetValueByCmd(const TString& cmd, const TString& value);
  virtual void Parse(const char* filename);
  void Simulate();

  Bool_t SubmitTest();

  ClassDefOverride(TJAlienJDL, 1)  // Creates JDL files for the AliEn middleware
};

#endif
