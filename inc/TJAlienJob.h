/*
 * TJAlienJob.h
 *
 *  Created on: Aug 13, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienJob
#define ROOT_TJAlienJob

#ifndef ROOT_TGridJob
#include "TGridJob.h"
#endif

class TJAlienJob : public TGridJob {
public:
  explicit TJAlienJob(const TString& jobID) : TGridJob(jobID) {}

  ~TJAlienJob() override = default;

  TGridJobStatus* GetJobStatus() const override;
  Bool_t Resubmit() override;
  Bool_t Cancel() override;

  ClassDefOverride(TJAlienJob, 1)  // JAliEn implementation of TGridJob
};

#endif
